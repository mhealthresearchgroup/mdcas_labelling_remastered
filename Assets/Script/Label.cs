﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Label {
	public DateTime createdTime;
	public float startTime, endTime, timeStamp;
	public string createdByUser;
	public string labelType;

	public Label(float timeStamp, float startTime, float endTime, string labelType, string createdByUser, DateTime createdTime){
		this.startTime = startTime;
		this.endTime = endTime;
		this.labelType = labelType;
		this.timeStamp = timeStamp;
		this.createdByUser = createdByUser;
		this.createdTime = createdTime;
	}

}


//public class Label {
//	public DateTime start, end;
//	public string labelName;
//	public long durationTicks, startTicks, endTicks;
//	// Use this for initialization
//	public Label(string start, string end){
//		this.start = parseStringDate (start);
//		this.end = parseStringDate (end);
//
//		this.startTicks = this.start.Ticks;
//		this.endTicks = this.end.Ticks;
//
//		this.durationTicks = this.endTicks - this.startTicks;
//	}
//
//	public Label(string start, string end, string labelName){
//		this.start = parseStringDate (start);
//		this.end = parseStringDate (end);
//
//		this.startTicks = this.start.Ticks;
//		this.endTicks = this.end.Ticks;
//		this.labelName = labelName;
//
//		this.durationTicks = this.endTicks - this.startTicks;
//	}
//		
//
//	public DateTime parseStringDate(string dateString){
//		DateTime date = DateTime.Parse (dateString);
//		return date;
//	}
//}
