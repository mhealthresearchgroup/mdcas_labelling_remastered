﻿using UnityEngine;
using System.Collections;

public class ModelController: MonoBehaviour {
	public int stop = 1, current = 1, start = 1;
	public Vector3 moveScale = new Vector3(1, 1, 1);
	private Vector3 speed;
	public float deltaTime = 0.01f;
	private float maxX = 0, maxY = 0, maxZ = 0;
	// Use this for initialization
	void Start () {
		speed = Vector3.zero;
		//rigidbody.AddForce (new Vector3 (-10, 0, 0), ForceMode.Acceleration);
//		StartCoroutine (test ());
//		56160 56880 
		//GetComponent<Rigidbody>().AddForce (new Vector3(0, 0.3f, 0));
	}

	public void init(int start, int stop, Vector3 movescale){
		speed = Vector3.zero;
		this.start = start;
		this.current = start;
		this.stop = stop;
		this.moveScale = movescale;
		//Debug.Log("start, stop = ( " + start + ", " + stop + ");");
		transform.position = new Vector3 (-2.5f, 1.1f, 49.4f);
		GetComponent<Rigidbody>().velocity = Vector3.zero;
		return;
	}

	IEnumerator test(){
		yield return new WaitForSeconds (3f);
		//rigidbody.AddForce (new Vector3 (10, 0, 0), ForceMode.Acceleration);
	}
	// Update is called once per frame
	void Update () {

	}

	private int tmp = 0;
	void FixedUpdate(){
	//	Debug.Log (GetComponent<Rigidbody> ().velocity);

		//test
//		Vector3 a = new Vector3 (0, 1, 0);
//		if (tmp < 100) {
//			Debug.Log (tmp++);
//			GetComponent<Rigidbody>().AddForce (a);
//		}
//		else GetComponent<Rigidbody>().AddForce (Vector3.zero);
//		Debug.Log (GetComponent<Rigidbody> ().velocity);
//		current ++;

		if (current >= stop) {
			Debug.Log ("max acceleration: " + new Vector3 (maxX, maxY, maxZ));
			init (start, stop, moveScale);
			return;
		}

		float scale = 10;
//		Vector3 acceleration = new Vector3(
//			float.Parse(Global.dataGrid[2, current]) * moveScale.y * scale, 
//			float.Parse(Global.dataGrid[3, current]) * moveScale.z * scale,
//			float.Parse(Global.dataGrid[1, current]) * moveScale.x * scale
//			);

		Vector3 acceleration = getAccleration (current);

		if (Mathf.Abs (acceleration.x) > maxX)
			maxX = Mathf.Abs (acceleration.x);
		if (Mathf.Abs (acceleration.y) > maxY)
			maxY = Mathf.Abs (acceleration.y);
		if (Mathf.Abs (acceleration.z) > maxZ)
			maxZ = Mathf.Abs (acceleration.z);
		
		acceleration.x = acceleration.x * moveScale.x;
		acceleration.y = acceleration.y * moveScale.y;
		acceleration.z = acceleration.z * moveScale.z;
		Vector3 deltaPos = speed * deltaTime + 0.5f * acceleration * deltaTime * deltaTime;
		speed = speed + acceleration * deltaTime;
		Vector3 currentPos = this.gameObject.transform.position;
		this.gameObject.transform.position = currentPos + deltaPos * 100;
			
//		Vector3 acceleration = new Vector3(
//			0, 
//			float.Parse(Global.dataGrid[3, current]),
//			0
//		            );
//		Vector3 acceleration = new Vector3(
//			0, 
//			float.Parse(Global.dataGrid[3, current]),
//			0
//			);

		//Debug.Log ("a: " + acceleration +　"v: " + speed);
		//GetComponent<Rigidbody>().AddForce (acceleration);
		current ++;

	}
		
	Vector3 getAccleration(int id){
		return new Vector3 (
			float.Parse(Global.dataGrid [2, current]), 
			float.Parse(Global.dataGrid [3, current]), 
			float.Parse(Global.dataGrid [1, current])
		);
	}

}


