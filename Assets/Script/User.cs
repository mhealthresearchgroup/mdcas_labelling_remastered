﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class User {

	public string startTime, endTime, labelName, playerID, createdTime, updatedTime;
	public bool gameMode;
	public User(){}

	public User(DateTime startTime, DateTime endTime, string labelName, string playerID, bool gameMode){
		string time = startTime.ToString ();
		time = time.Replace ("/", "_");
		this.startTime = time;

		time = endTime.ToString ();
		time = time.Replace ("/", "_");
		this.endTime = time;
		this.labelName = labelName;
		this.playerID = playerID;
		DateTime date = System.DateTime.Now;
		string dateStr;
		dateStr = date.ToString ();
		dateStr = dateStr.Replace ("/", "_");
		this.createdTime = dateStr;
		this.updatedTime = dateStr;
		this.gameMode = gameMode;
	}
}
