﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class RangeGraphOnObject : MonoBehaviour
{
    private loadDataThread dataThread = new loadDataThread();
    public List<string> filePath;
    public float graphLeftBorder, graphRightBorder;
    private int oldHourIndex = 0;
    public int monthIndex = 6;
    public int dayIndex = 1;
    // Use this for initialization

    public GameObject timePrefab;
    public List<GameObject> timeLabelList = new List<GameObject>();
    private Slider speedSlider;


    void Start()
    {
        filePath.Clear();
        filePath.Add(getFilePathByDay(monthIndex, dayIndex));
        Debug.Log(getFilePathByDay(monthIndex, dayIndex));
        dataThread.pathList = filePath;
        dataThread.dayIndex = dayIndex;
        dataThread.graphLeftBorder = graphLeftBorder;
        dataThread.graphRightBorder = graphRightBorder;
        dataThread.yScale = 50f;
        dataThread.Start();

        myCanvas = GetComponentInParent<Canvas>();
        timePrefab = Resources.Load("timeLabel") as GameObject;


        oldHour = getHourAtPosition(0);
        createTimeLabels(0, 24, 13);

        speedSlider = GameObject.Find("SpeedSlider").GetComponent<Slider>();
    }

    // redraw the graph when moving or scaling
    //render graph
    public float offset = 0, scale = 1, scale1 = 1;
    private Canvas myCanvas;
    public float moveSpeed = 0;
    private int oldHour;

    void Update()
    {
        offset -= Time.deltaTime * speedSlider.value / scale;
        //		scale1 += 0.01f;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (1.1 * scale < 9000)
                scale1 *= 1 + 0.1f;
            Vector3 scaleVtr = this.gameObject.GetComponent<RectTransform>().localScale;
            scaleVtr.x = scale1;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            scale1 *= 0.9f;
            Vector3 scaleVtr = this.gameObject.GetComponent<RectTransform>().localScale;
            scaleVtr.x = scale1;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            offset -= 10 / scale;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            offset += 10 / scale;
        }




        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {

            if (Input.GetKey(KeyCode.Q))
            {
                if (scale1 * Input.GetAxis("Mouse ScrollWheel") < 9000)
                    scale1 *= 1 + Input.GetAxis("Mouse ScrollWheel");

                Vector3 scaleVtr = this.gameObject.GetComponent<RectTransform>().localScale;
                scaleVtr.x = scale1;
                //				this.gameObject.GetComponent<RectTransform> ().localScale = scaleVtr;


            }
            if (Input.GetKey(KeyCode.E))
            {
                offset += 100 / scale1 * Input.GetAxis("Mouse ScrollWheel");
            }

        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            offset = 0;
        }

        if (
            (graphLeftBorder + offset) * scale > renderBorder ||
            (graphRightBorder + offset) * scale < -renderBorder
        )
        {
            scale = scale1;
            return;
        }




        scale = scale1;
        //reload data

        // show the time labels
        if (scale > 10)
        {
            int hour = getHourAtPosition(0);
            int startHour = Mathf.Max(0, hour - 2);
            int endHour = Mathf.Min(24, hour + 2);
            if (oldHour != hour)
            {
                //				Debug.Log (hour);
                createTimeLabels(startHour, endHour, (endHour - startHour) * 4 + 1);
                oldHour = hour;
            }
            else updateTimeLabels(startHour, endHour, (endHour - startHour) * 4 + 1);
        }
        if (scale > 1.4 && scale < 10)
            updateTimeLabels(0, 24, 25);
        if (scale < 1.4 && scale > 0.6)
            updateTimeLabels(0, 24, 13);
        if (scale < 0.6)
            updateTimeLabels(0, 24, 5);



    }

    int getHourAtPosition(float posX)
    {
        float hour = 24 * (posX - (graphLeftBorder + offset) * scale1) / ((graphRightBorder - graphLeftBorder) * scale1);

        hour = Mathf.Max(hour, 0);
        hour = Mathf.Min(hour, 24);

        return Mathf.FloorToInt(hour);
    }

    int getMinuteAtPosition(float posX)
    {
        float minute = 24 * 60 * (posX - (graphLeftBorder + offset) * scale1) / ((graphRightBorder - graphLeftBorder) * scale1);

        minute = Mathf.Max(minute, 0);
        minute = Mathf.Min(minute, 24 * 60);

        return Mathf.FloorToInt(minute);
    }

    //create line material
    public int lineCount = 100;
    public float radius = 3.0f;

    static Material lineMaterial;
    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }


    int renderBorder = 800;
    public void OnRenderObject()
    {
        if (!dataThread.IsDone)
            return;

        if ((graphLeftBorder + offset) * scale > renderBorder)
            return;
        if ((graphRightBorder + offset) * scale < -renderBorder)
            return;


        CreateLineMaterial();
        // Apply the line material
        lineMaterial.SetPass(0);

        GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        GL.MultMatrix(transform.localToWorldMatrix);

        // Draw the line-graph based on the signal data
        GL.Begin(GL.LINES);

        int m, dataLength;
        //		Debug.Log (getHourAtPosition (0));


        m = 0;
        dataLength = dataThread.dataLength;


        for (; m < dataLength - 4; m += 2)
        {
            GL.Begin(GL.QUADS);
            float x1 = dataThread.dataGrid[m, 0];

            float y1max = dataThread.dataGrid[m, 1];
            float y1min = dataThread.dataGrid[m + 1, 1];

            float x2 = dataThread.dataGrid[m + 2, 0];

            float y2max = dataThread.dataGrid[m + 2, 1];
            float y2min = dataThread.dataGrid[m + 3, 1];

            GL.Color(new Color(0.35f, 0.6f, 0, 0.4F));
            GL.Vertex3((x1 + offset) * scale1, y1max, 0);
            GL.Vertex3((x1 + offset) * scale1, y1min, 0);
            GL.Vertex3((x2 + offset) * scale1, y2min, 0);
            GL.Vertex3((x2 + offset) * scale1, y2max, 0);

            GL.End();

            GL.Begin(GL.QUADS);

            y1max = dataThread.dataGrid[m, 2];
            y1min = dataThread.dataGrid[m + 1, 2];

            y2max = dataThread.dataGrid[m + 2, 2];
            y2min = dataThread.dataGrid[m + 3, 2];
            GL.Color(new Color(0.95f, 0.5f, 0.2f, 0.4F));
            GL.Vertex3((x1 + offset) * scale1, y1max, 0);
            GL.Vertex3((x1 + offset) * scale1, y1min, 0);
            GL.Vertex3((x2 + offset) * scale1, y2min, 0);
            GL.Vertex3((x2 + offset) * scale1, y2max, 0);

            GL.End();

            GL.Begin(GL.QUADS);

            y1max = dataThread.dataGrid[m, 3];
            y1min = dataThread.dataGrid[m + 1, 3];

            y2max = dataThread.dataGrid[m + 2, 3];
            y2min = dataThread.dataGrid[m + 3, 3];

            GL.Color(new Color(0f, 0.5f, 0.5f, 0.4F));
            GL.Vertex3((x1 + offset) * scale1, y1max, 0);
            GL.Vertex3((x1 + offset) * scale1, y1min, 0);
            GL.Vertex3((x2 + offset) * scale1, y2min, 0);
            GL.Vertex3((x2 + offset) * scale1, y2max, 0);

            GL.End();
            //
            //			GL.Color(new Color(0.35f, 0.6f, 0, 0.8F));
            //			// One vertex at transform position
            //			GL.Vertex3((x1 + offset) * scale1, y1, 0);
            //			// Another vertex at edge of circle
            //			GL.Vertex3((x2 + offset) * scale1, y2, 0);
            //
            //			y1 = dataThread.dataGrid[m, 2];
            //			y2 = dataThread.dataGrid[m+1, 2];
            //
            //			GL.Color(new Color(0.95f, 0.5f, 0.2f, 0.8F));
            //			// One vertex at transform position
            //			GL.Vertex3((x1 + offset) * scale1, y1, 0);
            //			// Another vertex at edge of circle
            //			GL.Vertex3((x2 + offset) * scale1, y2, 0);
            //
            //
            //			y1 = dataThread.dataGrid[m, 3];
            //			y2 = dataThread.dataGrid[m+1, 3];
            //
            //			GL.Color(new Color(0f, 0.5f, 0.5f, 0.8F));
            //			// One vertex at transform position
            //			GL.Vertex3((x1 + offset) * scale1, y1, 0);
            //			// Another vertex at edge of circle
            //			GL.Vertex3((x2 + offset) * scale1, y2, 0);

            GL.End();

        }

        //		float labelY;
        //		for (int i = 0; i < 24; i++) {
        //			if (i % 2 == 0){
        //				GL.Color(new Color(1f, 0f, 0, 0.8F));
        //				labelY = -120;
        //			}
        //			else{
        //				GL.Color(new Color(0f, 0f, 1, 0.8F));
        //				labelY = -150;
        //			}
        //			
        //			GL.Vertex3((-400 + 800 * i / 24) * scale + offset , labelY, 0);
        //			GL.Vertex3((-400 + 800 * (i+1) / 24) * scale + offset, labelY, 0);
        //		}


        //label
        //		if (Global.labelLengthOfDay [4] < 10)
        //			return;
        //
        //		for (int i = 0; i < Global.labelLengthOfDay [4]; i++) {
        //			float x1 = Global.labelOfDay [4] [i, 1] * scale + offset;
        //			float x2 = Global.labelOfDay [4] [i, 2] * scale + offset;
        //			float y = -100;
        //			if (i % 2 == 0)
        //				y = -130;
        //			float color = Global.labelOfDay [4] [i, 3];
        //			GL.Color (new Color (color, 1 - color, 1, 1));
        //			GL.Vertex3 (x1, y, 0);
        //			GL.Vertex3 (x2, y, 0);
        //		}




        //Draw coordinate
        GL.Color(new Color(0f, 0f, 1, 0.8F));

        GL.Vertex3((graphLeftBorder + offset) * scale1, 0, 0);
        GL.Vertex3((graphRightBorder + offset) * scale1, 0, 0);

        float deltaRange = (graphRightBorder - graphLeftBorder) / (6f * 24f);

        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                float x = graphLeftBorder + (i * 6 + j) * deltaRange;
                x = (x + offset) * scale1;
                //Grid for one hour
                if (j == 0)
                {
                    GL.Vertex3(x, 10, 0);
                    GL.Vertex3(x, -10, 0);
                }
                else
                {
                    //Grid for 10 min
                    if (scale1 > 2)
                    {
                        GL.Vertex3(x, 7, 0);
                        GL.Vertex3(x, -7, 0);
                    }
                }
            }
        }

        GL.Color(new Color(0f, 0f, 1f, 0.8F));

        //Grid for 2min
        deltaRange = (graphRightBorder - graphLeftBorder) / (6f * 24f * 5f);
        if (scale1 > 10)
        {
            for (int i = 0; i < 24; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    for (int k = 1; k < 5; k++)
                    {
                        float x = graphLeftBorder + (i * 6 * 5 + j * 5 + k) * deltaRange;
                        x = (x + offset) * scale1;
                        GL.Vertex3(x, 3, 0);
                        GL.Vertex3(x, -3, 0);
                    }
                }
            }
        }
        GL.End();



        // draw colored area as a label
        //		GL.Begin(GL.QUADS);
        //		for (int i = 0; i < Global.labelLengthOfDay [4]; i++) {
        //			float x1 = Global.labelOfDay [4] [i, 1] * scale + offset;
        //			float x2 = Global.labelOfDay [4] [i, 2] * scale + offset;
        //			float y1 = -100;
        //			float y2 = 100;
        //			float color = Global.labelOfDay [4] [i, 3];
        //			GL.Color (new Color (color, 1 - color, 1, 0.1f));
        //			GL.Vertex3 (x1, y1, 0);
        //			GL.Vertex3 (x1, y2, 0);
        //			GL.Vertex3 (x2, y2, 0);
        //			GL.Vertex3 (x2, y1, 0);
        //
        //		}
        //		GL.End ();

        GL.Begin(GL.QUADS);
        GL.Color(new Color(1, 1, 1, 0.1f));
        GL.Vertex3((graphLeftBorder + offset) * scale1, 130, 0);
        GL.Vertex3((graphLeftBorder + offset) * scale1, -130, 0);
        GL.Vertex3((graphRightBorder + offset) * scale1, -130, 0);
        GL.Vertex3((graphRightBorder + offset) * scale1, 130, 0);
        GL.End();

        GL.PopMatrix();

    }

    string getFilePathByDay(int month, int day)
    {
        string dayStr = day.ToString("00");
        string monthStr = month.ToString("00");
        return "data/" + dayStr + "/Day-RangeData400-Data-2017-" + monthStr + "-" + dayStr + ".csv";

    }

    string getFilePathByDayHour(int month, int day, int hour)
    {
        string dayStr = day.ToString("00"), hourStr = hour.ToString("00");
        string monthStr = month.ToString("00");
        return "data/" + dayStr + "/" + hourStr + "/AvgData080-Data-2017-" + monthStr + "-" + dayStr + "-" + hourStr + ".csv";
    }

    string getAvgFilePathByDayHourAndSamplingRate(int month, int day, int hour, int samplingRate)
    {
        string dayStr = day.ToString("00"), hourStr = hour.ToString("00"), rateStr = samplingRate.ToString("000");
        string monthStr = month.ToString("00");
        return "data/" + dayStr + "/" + hourStr + "/AvgData" + rateStr + "-Data-2017-" + monthStr + "-" + dayStr + "-" + hourStr + ".csv";
    }

    string getRawFilePathByDayHour(int month, int day, int hour)
    {
        string dayStr = day.ToString("00"), hourStr = hour.ToString("00");
        string monthStr = month.ToString("00");
        return "data/" + dayStr + "/" + hourStr + "/RawDataFloat-2017-" + monthStr + "-" + dayStr + "-" + hourStr + ".csv";
    }

    void createTimeLabels(int startHourIndex, int endHourIndex, int labelNum)
    {
        destroyTimeLabels();
        if (labelNum <= 1)
            return;

        float deltaHourRange = (graphRightBorder - graphLeftBorder) / 24;
        float startPosX = graphLeftBorder + startHourIndex * deltaHourRange;
        float delta = (endHourIndex - startHourIndex) / 24f * (graphRightBorder - graphLeftBorder) / (float)(labelNum - 1);
        float labelDelta = (endHourIndex - startHourIndex) / (float)(labelNum - 1);

        //		Debug.Log(startHourIndex + ":" + endHourIndex + ":" + startPosX);
        // Add each lanbel. 
        for (int i = 0; i < labelNum; i++)
        {
            // Init timeLabel
            GameObject timeLabel = Instantiate(timePrefab, myCanvas.transform, false);
            timeLabel.transform.SetParent(this.gameObject.transform);
            // Set Position
            float posX = startPosX + i * delta;
            posX = (posX + offset) * scale;
            timeLabel.transform.localPosition = new Vector3(posX, -120, 0);

            // Set TimeString
            DateTime date = new DateTime();
            float time = startHourIndex + i * labelDelta;
            date = date.AddHours(time);
            //			Debug.Log (date.ToLongTimeString ());
            timeLabel.GetComponent<Text>().text = (date.Hour).ToString("D2") + ":" + (date.Minute).ToString("D2");
            //
            //Add to label list
            timeLabelList.Add(timeLabel);
        }

    }

    void destroyTimeLabels()
    {
        foreach (GameObject timeLabel in timeLabelList)
        {
            Destroy(timeLabel);
        }
        timeLabelList.Clear();
    }

    void updateTimeLabels(int startHourIndex, int endHourIndex, int labelNum)
    {
        if ((graphLeftBorder + offset) * scale > renderBorder || (graphRightBorder + offset) * scale < -renderBorder)
            return;

        if (labelNum <= 1 || timeLabelList.Count <= 1)
            return;
        if (timeLabelList.Count != labelNum)
            createTimeLabels(startHourIndex, endHourIndex, labelNum);

        float deltaHourRange = (graphRightBorder - graphLeftBorder) / 24f;
        float startPosX = graphLeftBorder + startHourIndex * deltaHourRange;
        float delta = (endHourIndex - startHourIndex) / 24f * (graphRightBorder - graphLeftBorder) / (float)(labelNum - 1);

        for (int i = 0; i < timeLabelList.Count; i++)
        {
            float posX = startPosX + i * delta;
            posX = (posX + offset) * scale;
            timeLabelList[i].transform.localPosition = new Vector3(posX, -120, 0);

        }
    }
}
