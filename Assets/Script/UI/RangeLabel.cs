﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class RangeLabel : MonoBehaviour {
	private Vector2 mousePos = Vector2.zero;
	public List<Button> buttonList;
	public RangeGraphOnObject sampleGraph;

	private float[,] labels = new float[10000, 3];
	private float[,] trueLabels = new float[10000, 4];
	private int labelLength = 0, trueLabelLength = 0;
	private int labelIndex = 0;

	private loadLabelThread labelThread = new loadLabelThread();
	public List<string> filePath;

	// Use this for initialization
	void Start () {
		string labelFilePath = getLabelFilePathByDayHour (sampleGraph.monthIndex, sampleGraph.dayIndex);
		filePath.Clear ();
		filePath.Add (labelFilePath);

		loadCsvLabelGrid (labelFilePath, ref labelIndex, ref trueLabelLength, ref trueLabels);
		scaleDataToFitGraph (sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, ref trueLabels);

	}

	public Canvas myCanvas;
	// Update is called once per frame
	private float offset = 30f;
	private int day;
	private float time;

	void Update () {
		if (Input.GetKey (KeyCode.P)) {
			foreach (string label in Global.labelType) {
				Debug.Log (label);
			}
			Debug.Log (trueLabels[0, 0] + ", " + trueLabels[0, 1] + ", " + trueLabels[0, 2] + ", " + trueLabels[0, 3]);
		}


		if (Input.GetMouseButton(0)) {
			if (myCanvas) {
				Vector2 pos;
				RectTransformUtility.ScreenPointToLocalPointInRectangle (myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);
				if (Mathf.Abs (pos.y) < 100) {
					mousePos = pos;
					float xPos = mousePos.x / sampleGraph.scale - sampleGraph.offset;
					//get day
					day = Mathf.FloorToInt((xPos - sampleGraph.graphLeftBorder) / (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder));
					//get time
					time = (xPos - sampleGraph.graphLeftBorder) % (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder);
					time = time / (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * 3600 * 24;
					//Debug.Log (time);
				}
				//Debug.Log (mousePos);
				if (buttonList.Capacity > 0){
					for (int i = 0; i < buttonList.Capacity; i++) {
						buttonList [i].transform.localPosition = new Vector2(mousePos.x, -150 - offset * i);
					}
				}
			}
		}
	}

	public void addLabel(int type){
		if (day < 0)
			return;

		int i = labelLength;
		labels [i, 0] = day;
		labels [i, 1] = time;
		labels [i, 2] = type;

		float labelPosX = labels [i, 0] * (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) + sampleGraph.graphLeftBorder;
		labelPosX += labels [i, 1] / (3600 * 24) * (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder);
		labelPosX = (labelPosX + sampleGraph.offset) * sampleGraph.scale;
		Debug.Log (day);
		Debug.Log (time);
		Debug.Log (labelPosX);
		labelLength++;

	}
	public void OnRenderObject()
	{
		float scale = sampleGraph.scale1, offset = sampleGraph.offset;
		CreateLineMaterial();
		// Apply the line material
		lineMaterial.SetPass(0);
		GL.PushMatrix();
		// Set transformation matrix for drawing to
		// match our transform
		GL.MultMatrix(transform.localToWorldMatrix);

		// Draw the line-graph based on the signal data
		GL.Begin(GL.LINES);

		GL.Color(new Color(1f, 0f, 0, 1F));

		//draw SignalLine
		GL.Vertex3(mousePos.x-10, 130, 1);
		GL.Vertex3(mousePos.x+10, 130, 1);
		GL.Vertex3(mousePos.x-10, -130, 1);
		GL.Vertex3(mousePos.x+10, -130, 1);

		GL.Vertex3(mousePos.x, 130, 1);
		GL.Vertex3(mousePos.x, -130, 1);



		//draw label
		//		for (int i = 0; i < labelLength; i++){
		//			float labelPosX = labels [i, 0] * (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) + sampleGraph.graphLeftBorder;
		//			labelPosX += labels [i, 1] / (3600 * 24) * (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder);
		//
		//			int type = (int)labels [i, 2];
		//			if (type == 0)
		//				GL.Color(new Color(1f, 0f, 0, 1F));
		//			if (type == 1)
		//				GL.Color(new Color(0f, 1f, 0, 1F));
		//			if (type == 2)
		//				GL.Color(new Color(0f, 0f, 1, 1F));
		//			GL.Vertex3((labelPosX + offset) * scale, 130, 1);
		//			GL.Vertex3((labelPosX + offset) * scale, -130, 1);
		//
		//		}
		GL.End();

		//draw true labels
		for (int i = 0; i < trueLabelLength; i++){
			float labelPosX = trueLabels [i, 0];
			//			Debug.Log ((labelPosX + offset) * scale);


			GL.Begin (GL.QUADS);

			GL.Color (Global.colorList [Mathf.RoundToInt(trueLabels [i, 3])]);


			GL.Vertex3((trueLabels [i, 1] + offset) * scale, 130, 1);
			GL.Vertex3((trueLabels [i, 1] + offset) * scale, -130, 1);
			GL.Vertex3((trueLabels [i, 2] + offset) * scale, -130, 1);
			GL.Vertex3((trueLabels [i, 2] + offset) * scale, 130, 1);
			GL.End ();

		}

		GL.PopMatrix();



	}



	static Material lineMaterial;
	static void CreateLineMaterial()
	{
		if (!lineMaterial)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things.
			Shader shader = Shader.Find("Hidden/Internal-Colored");
			lineMaterial = new Material(shader);
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			// Turn on alpha blending
			lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			// Turn backface culling off
			lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			// Turn off depth writes
			lineMaterial.SetInt("_ZWrite", 0);
		}
	}

	string getLabelFilePathByDayHour(int month, int day){
		string dayStr = day.ToString("00");
		string monthStr = month.ToString("00");
		return "data/" + dayStr + "/Label-2017-" + monthStr + "-" + dayStr + ".csv";  
	}

	void loadCsvLabelGrid(string filePath, ref int index, ref int length,ref float[,] dataGrid){
		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 

		//data
		string[] data;//tmp

		for (; !reader.EndOfStream && index < dataGrid.Length; index++) {

			data = reader.ReadLine ().Split (',');

			dataGrid[index, 0] = float.Parse (data [0]);
			dataGrid[index, 1] = float.Parse (data [1]);
			dataGrid[index, 2] = float.Parse (data [2]);

			string label = data [3];
			//
			//			int i;

			//
			//			if (i == Global.labelTypeCount) {
			//				Global.labelType [i] = label;
			//				Global.labelTypeCount++;
			//				dataGrid [index, 3] = i;
			//			}
			if (!Global.labelType.Contains(label)){
				Global.labelType.Add (label);
			}

			for (int i = 0; i < Global.labelType.Count; i++) {
				if (label == Global.labelType[i])
					dataGrid [index, 3] = i;
			}

			//			 = (float)Global.labelType.fi
			length++;
		}

		reader.Close ();
		return;

	}

	public void scaleDataToFitGraph(float leftBorder, float rightBorder, ref float[,] dataGrid){
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
		for (int i = 0; i < trueLabelLength; i++) {
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
			dataGrid [i, 0] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 0] / (3600f * 24);
			dataGrid [i, 1] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 1] / (3600f * 24);
			dataGrid [i, 2] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 2] / (3600f * 24);
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
		}
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
	}
}
