﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleDisplay : MonoBehaviour {
	public Text text;
	public RenderGraphOnObject graph;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		text.text = graph.scale.ToString("F1");
	}
}
