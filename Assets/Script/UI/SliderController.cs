﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour {
	public Text valueText;
	// Use this for initialization
	void Start () {
		valueText.text = this.GetComponent<Slider> ().value.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void valueChanged(){
		valueText.text = this.GetComponent<Slider> ().value.ToString();
	}

	public void changeGraphScale(RenderGraphOnObject graph){
		graph.scale1 = this.GetComponent<Slider> ().value;
//		Debug.Log (graph.scale1);
	}
}
