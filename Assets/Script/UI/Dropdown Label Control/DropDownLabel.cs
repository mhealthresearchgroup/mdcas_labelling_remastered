﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum LabelType
{
    Sedentary,
    Standing,
    Ambulatory,
    Lifestyle,
    Other,
    Invalid,
}

public class DropDownLabel : Singleton<DropDownLabel>
{
    public RenderLabel labelRenderer;
    Dropdown dropdown;
    private void Start()
    {
        StartCoroutine(LoadData());

    }

    private void Update()
    {
        if (dropdown == null || labelRenderer == null) return;
        if (dropdown.transform.childCount >4)
        {
            // open
            SetAbleToChoosePosition(false);
        }
        else
            // Set the label renderer able to choose label position again.
            SetAbleToChoosePosition(true);
    }

    public int Value
    {
        get { return dropdown.value; }
        set { dropdown.value = value; }
    }

    public int DefaultOptionValue { get { return dropdown.options.Count - 1; } }

    public void DropdownValueChanged(Dropdown change)
    {
        var value = change.value;
        if (value != dropdown.options.Count - 1)
        {
            ChangeDropdownColor(value);
            labelRenderer.addLabel(value);
        }
        else
        {
            ChangeDropdownColor(-1);
        }


    }

    void ChangeDropdownColor(int colorIndex)
    {
        Color color;
        if (colorIndex == -1)
            color = Color.white;
        else
            color = Global.colorList[colorIndex];

        var colors = dropdown.colors;
        colors.normalColor = color;
        colors.highlightedColor = color;
        dropdown.colors = colors;
    }

    public IEnumerator LoadData()
    {
        yield return new WaitUntil(() => Global.labelType.Count > 0);
        dropdown = GetComponent<Dropdown>();
        dropdown.options.Clear();
        for (int labelIndex = 0; labelIndex < Global.labelType.Count; labelIndex++)
        {
            var newOption = new Dropdown.OptionData()
            {
                text = Global.labelType[labelIndex],
            };
            dropdown.options.Add(newOption);
        }

        // Default option
        var defaultOption = new Dropdown.OptionData()
        {
            text = "Select a label",
        };
        dropdown.options.Add(defaultOption);

        // select the default option
        dropdown.value = dropdown.options.Count - 1;
        ChangeDropdownColor(-1);
    }

    public void SetAbleToChoosePosition(bool value)
    {
        labelRenderer.canUpdatePointerPosition = value;
    }
}

////public class DropDownLabel : MonoBehaviour
//{
//    public List<Dropdown> dropdowns;
//    Dropdown dropdown;
//    //LabelType prevLabel = LabelType.Invalid;
//    public RenderLabel labelRenderer;
//    public LabelType type;
//    public bool toAddLabel = true;

//    void Start()
//    {
//        dropdown = GetComponent<Dropdown>();
//        dropdown.onValueChanged.AddListener(delegate
//        {
//            DropdownValueChanged(dropdown);
//        });
//        dropdowns = new List<Dropdown>(transform.parent.GetComponentsInChildren<Dropdown>());
//    }

//    //Ouput the new value of the Dropdown into Text
//    void DropdownValueChanged(Dropdown change)
//    {
//        //if (prevLabel == type) return;

//        if (toAddLabel == false)
//        {
//            toAddLabel = true;
//            return;
//        }

//        switch (type)
//        {
//            case LabelType.Sedentary:
//                labelRenderer.addLabel(0);
//                break;
//            case LabelType.Standing:
//                labelRenderer.addLabel(2);
//                break;
//            case LabelType.Ambulatory:
//                labelRenderer.addLabel(1);
//                break;
//            case LabelType.Lifestyle:
//                labelRenderer.addLabel(3);
//                break;
//            case LabelType.Other:
//                labelRenderer.addLabel(4);
//                break;
//        }

//        foreach (var dd in dropdowns)
//        {
//            if (dd.Equals(dropdown)) continue;
//            dd.GetComponent<DropDownLabel>().toAddLabel = false;
//            dd.value = 0;
//        }
//        foreach (var dd in dropdowns)
//        {
//            dd.GetComponent<DropDownLabel>().toAddLabel = true;
//        }
//        //prevLabel = type;
//    }
//}
