﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DateUI : MonoBehaviour {
	private Text text;
	public RenderGraphOnObject sampleGraph;
	// Use this for initialization
	void Start () {
		text = this.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		float dayF = ((0 - sampleGraph.offset) - sampleGraph.graphLeftBorder) / (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder);
		dayF += 12;
		int day = Mathf.CeilToInt (dayF);

		int samplingRate = 1000;
		float scale = sampleGraph.scale;

		if (scale < 8)
			samplingRate = 1000;
		
		if (scale > 8 && scale < 80)
			samplingRate = 100;
		
		if (scale > 80 && scale < 700)
			samplingRate = 10;
		
		if (scale > 700)
			samplingRate = 1;
		
		text.text = sampleGraph.monthIndex + "/" + sampleGraph.dayIndex + "\nSampling Rate: 1:" + samplingRate;
	}
}
