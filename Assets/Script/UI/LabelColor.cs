﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelColor : MonoBehaviour {
	static public LabelColor ins;

	public List<Color> colorList;

	void Awake(){
		ins = this;
	}
	// Use this for initialization
	void Start () {
		Global.colorList = colorList;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
