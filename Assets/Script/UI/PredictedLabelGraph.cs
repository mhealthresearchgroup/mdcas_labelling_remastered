﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class PredictedLabelGraph : MonoBehaviour {
	public RenderGraphOnObject sampleGraph;
	public List<string> filePath;
	public float yScale = 100, yZeroLine = 150;

	private float[,] predictedLabel = new float[10000, 4];
	private float[,] predictedProbability = new float[10000, 4];
	private int predictedLabelLength = 0, predictedLabelIndex = 0;
	private int predictedProbabilityLength = 0, predictedProbabilityIndex = 0;
	// Use this for initialization
	void Start () {
		//load label
		string labelPath = getPredictedLabelFilePathByDayHour (sampleGraph.monthIndex, sampleGraph.dayIndex);
		loadCsvPredictedLabelGrid (labelPath, ref predictedLabelIndex, ref predictedLabelLength, ref predictedLabel);
		scalePredictedLabelToFitGraph (sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, predictedLabelLength, ref predictedLabel);
		//load probability
		string probabilityPath = getPredictedProbabilityFilePathByDayHour (sampleGraph.monthIndex, sampleGraph.dayIndex);
		loadCsvProbabilityGrid (probabilityPath, ref predictedProbabilityIndex, ref predictedProbabilityLength, ref predictedProbability);
		scalePredictedLabelToFitGraph (sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, predictedProbabilityLength, ref predictedProbability);

	}

	public void init(){
		predictedLabel = new float[10000, 4];
		predictedProbability = new float[10000, 4];
		predictedLabelLength = 0;
		predictedLabelIndex = 0;
		predictedProbabilityLength = 0;
		predictedProbabilityIndex = 0;

		//load label
		string labelPath = getPredictedLabelFilePathByDayHour (sampleGraph.monthIndex, sampleGraph.dayIndex);
		loadCsvPredictedLabelGrid (labelPath, ref predictedLabelIndex, ref predictedLabelLength, ref predictedLabel);
		scalePredictedLabelToFitGraph (sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, predictedLabelLength, ref predictedLabel);
		//load probability
		string probabilityPath = getPredictedProbabilityFilePathByDayHour (sampleGraph.monthIndex, sampleGraph.dayIndex);
		loadCsvProbabilityGrid (probabilityPath, ref predictedProbabilityIndex, ref predictedProbabilityLength, ref predictedProbability);
		scalePredictedLabelToFitGraph (sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, predictedProbabilityLength, ref predictedProbability);

		Debug.Log (predictedLabel [0, 0] + ":" + predictedLabel [0, 3]);
		Debug.Log (predictedLabel [3, 0] + ":" + predictedLabel [3, 3]);
	
	}

	static Material lineMaterial;
	static void CreateLineMaterial()
	{
		if (!lineMaterial)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things.
			Shader shader = Shader.Find("Hidden/Internal-Colored");
			lineMaterial = new Material(shader);
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			// Turn on alpha blending
			lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			// Turn backface culling off
			lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			// Turn off depth writes
			lineMaterial.SetInt("_ZWrite", 0);
		}
	}

	int renderBorder = 800;

	public void OnRenderObject()
	{
		CreateLineMaterial();
		// Apply the line material
		lineMaterial.SetPass(0);

		GL.PushMatrix();
		// Set transformation matrix for drawing to
		// match our transform
		GL.MultMatrix(transform.localToWorldMatrix);

		// Draw the line-graph based on the signal data
		GL.Begin(GL.QUADS);

		float offset = sampleGraph.offset, scale1 = sampleGraph.scale1;

		for (int i = 0; i < predictedLabelLength; i++) {
			float x1 = predictedLabel [i, 1];
			float x2 = predictedLabel [i, 2];
			int colorIndex = Mathf.RoundToInt (predictedLabel [i, 3]);
			float y = predictedProbability [i, 3];

			GL.Color(Global.colorList[colorIndex]);

			GL.Vertex3((x1 + offset) * scale1, y * yScale + yZeroLine, 1);
			GL.Vertex3((x2 + offset) * scale1, y * yScale + yZeroLine, 1);
			GL.Vertex3((x2 + offset) * scale1, 0 * yScale + yZeroLine, 1);
			GL.Vertex3((x1 + offset) * scale1, 0 * yScale + yZeroLine, 1);
		}
		GL.End();

		GL.Begin(GL.LINES);
		//draw coordinate
		GL.Color(new Color(0.5f, 0.5f, 0.5f, 0.5F));

		for (int i = 0; i <= 5; i++){
			GL.Vertex3((sampleGraph.graphLeftBorder + offset) * scale1, i * 0.2f * yScale + yZeroLine, 1);
			GL.Vertex3((sampleGraph.graphRightBorder + offset) * scale1, i * 0.2f * yScale  + yZeroLine, 1);
		}


		GL.End();
		GL.PopMatrix();
	}

	
	// Update is called once per frame
	void Update () {
		
	}



	string getPredictedLabelFilePathByDayHour(int month, int day){
		string dayStr = day.ToString("00");
		string monthStr = month.ToString("00");
		return "data/" + dayStr + "/PredictedLabel-binod-algorithm.2017-" 
            + monthStr + "-" + dayStr + "-M0500.annotation.csv";  
	}

	string getPredictedProbabilityFilePathByDayHour(int month, int day){
		string dayStr = day.ToString("00");
		string monthStr = month.ToString("00");
		return "data/" + dayStr + "/PredictedLabelProbability-binod-algorithm.2017-" 
            + monthStr + "-" + dayStr + "-M0500.annotation.csv";  
	}

	void loadCsvPredictedLabelGrid(string filePath, ref int index, ref int length,ref float[,] dataGrid){
		if (!File.Exists (filePath)) {
			Debug.Log ("Cannot find: " + filePath);
			return;
		}

		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 

		//data
		string[] data;//tmp

		for (; !reader.EndOfStream && index < dataGrid.Length; index++) {

			data = reader.ReadLine ().Split (',');

			dataGrid[index, 0] = float.Parse (data [0]);
			dataGrid[index, 1] = float.Parse (data [1]);
			dataGrid[index, 2] = float.Parse (data [2]);

			string label = data [3];
			//
			//			int i;

			//
			//			if (i == Global.labelTypeCount) {
			//				Global.labelType [i] = label;
			//				Global.labelTypeCount++;
			//				dataGrid [index, 3] = i;
			//			}

			if (!Global.labelType.Contains(label)){
				Global.labelType.Add (label);
			}

			for (int i = 0; i < Global.labelType.Count; i++) {
				if (label == Global.labelType[i]){
					dataGrid [index, 3] = i;
					break;
				}

			}

			//			 = (float)Global.labelType.fi
			length++;
		}

		reader.Close ();
		return;

	}

	void loadCsvProbabilityGrid(string filePath, ref int index, ref int length,ref float[,] dataGrid){
		if (!File.Exists (filePath)) {
			Debug.Log ("Cannot find: " + filePath);
			return;
		}

		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 

		//data
		string[] data;//tmp

		for (; !reader.EndOfStream && index < dataGrid.Length; index++) {

			data = reader.ReadLine ().Split (',');

			dataGrid[index, 0] = float.Parse (data [0]);
			dataGrid[index, 1] = float.Parse (data [1]);
			dataGrid[index, 2] = float.Parse (data [2]);
			dataGrid[index, 3] = float.Parse (data [3]);

			length++;
		}

		reader.Close ();
		return;

	}

	public void scalePredictedLabelToFitGraph(float leftBorder, float rightBorder, int dataLength, ref float[,] dataGrid){
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
		for (int i = 0; i < dataLength; i++) {
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
			dataGrid [i, 0] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 0] / (3600f * 24);
			dataGrid [i, 1] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 1] / (3600f * 24);
			dataGrid [i, 2] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 2] / (3600f * 24);
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
		}
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
	}


}
