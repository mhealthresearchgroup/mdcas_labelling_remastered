﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ButtonPrecompute : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	string path;
	// Update is called once per frame
	void Update () {
		
	}


	public void click(){
		summerizeOneDayLabels (Global.precomputedDay);
	}

	public void precompute(int dayIndex){
		string filepath = "data/Precomputed_" + dayIndex.ToString("D2") + ".csv";
		if (File.Exists (filepath)) {
			Debug.Log (filepath + " is already existed");
			File.Delete (filepath);
		}
		StreamWriter file = new StreamWriter (filepath);

		int oldDelta = 96;
		Vector3 sum = Vector3.zero;
		for (int hourIndex = 0; hourIndex < 24; hourIndex++){
			if (Global.lenthOfDayData [dayIndex] [hourIndex] < 10)
				continue;
			for (int i = 0; i < Global.lenthOfDayData [dayIndex][hourIndex]; i += oldDelta) {
				for (int j = i; j < i + oldDelta; j++) {
					sum += new Vector3 (Global.dataOfWeekGrid [dayIndex] [hourIndex] [j, 0], Global.dataOfWeekGrid [dayIndex] [hourIndex] [j, 1], Global.dataOfWeekGrid [dayIndex] [hourIndex] [j, 2]);
				}
				sum = sum / oldDelta;
				file.WriteLine (i / oldDelta + "," + sum.x + "," + sum.y + "," + sum.z);
				sum = Vector3.zero;
				//			list_x.Add (Global.list_x [i * oldDelta]);
				//			list_y.Add (Global.list_y [i * oldDelta]);
				//			list_z.Add (Global.list_z [i * oldDelta]);
			}
		}
		file.Close ();
		Debug.Log ("Day: " + dayIndex + " precomputed");

		Global.precomputedDay++;
	}

	public void summerizeOneDayLabels(int dayIndex){
		string filepath = "data/Label_" + dayIndex.ToString("D2") + ".csv";
		if (File.Exists (filepath)) {
			Debug.Log (filepath + " is already existed");
			File.Delete (filepath);
		}
		StreamWriter file = new StreamWriter (filepath);


		for (int hourIndex = 0; hourIndex < 24; hourIndex++){
			if (Global.labelOfHourDay[dayIndex][hourIndex][0,1] == null)
				continue;
			for (int i = 0; Global.labelOfHourDay[dayIndex][hourIndex][i,0] != null; i++) {
				file.WriteLine (
					Global.labelOfHourDay[dayIndex][hourIndex][i,0] + "," 
					+ Global.labelOfHourDay[dayIndex][hourIndex][i,1] + "," 
					+ Global.labelOfHourDay[dayIndex][hourIndex][i,2] + "," 
					+ Global.labelOfHourDay[dayIndex][hourIndex][i,3]
				);
			}
		}
		file.Close ();
		Debug.Log ("Day: " + dayIndex + " precomputed");

		Global.precomputedDay++;
	}
}
