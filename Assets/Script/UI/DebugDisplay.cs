﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugDisplay : Singleton<DebugDisplay>
{

    Text text;
     Button clearButton = null;
    static Text textStatic;

    public void LogFormat(string s, params object[] ps)
    {
        string l = string.Format(s, ps);
        text.text += l + System.Environment.NewLine;
    }

    public static void LogFormatStatic(string s, params object[] ps)
    {
        string l = string.Format(s, ps);
        textStatic.text += l + System.Environment.NewLine;
    }

    public void ClearLogs()
    {
        text.text = string.Empty;
    }

    public void ShowClearButton()
    {
        if (text.text != string.Empty)
        {
            if (clearButton) clearButton.gameObject.SetActive(true);
        }
    }

    public void HideClearButton()
    {
        if (clearButton) clearButton.gameObject.SetActive(false);
    }

    void Awake()
    {
        text = GetComponent<Text>();
        textStatic = text;
        ClearLogs();
    }

    void Update()
    {
        if(!clearButton)
        {
            clearButton = GetComponentInChildren<Button>();
            HideClearButton();
            return;
        }
    }
}
