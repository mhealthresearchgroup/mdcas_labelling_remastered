﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class RenderLabel : MonoBehaviour
{
    private Vector2 mousePos = Vector2.zero;
    public List<Button> buttonList;
    public RenderGraphOnObject sampleGraph;
    public FirebaseController firebase;
    public Toggle toggle;

    private float[,] labels = new float[10000, 4];
    private float[,] trueLabels = new float[10000, 4];
    private float[,] Rescaledlabels = new float[10000, 4];
    private int labelLength = 0, trueLabelLength = 0;
    private int labelIndex = 0;
    public bool ifShowTrueLabels = false;

    private loadLabelThread labelThread = new loadLabelThread();
    public List<string> filePath;

    // Use this for initialization
    void Start()
    {
        string labelFilePath = getLabelFilePathByDayHour(sampleGraph.yearIndex, sampleGraph.monthIndex, sampleGraph.dayIndex);
        filePath.Clear();
        filePath.Add(labelFilePath);

        loadCsvLabelGrid(labelFilePath, ref labelIndex, ref trueLabelLength, ref trueLabels);
        scaleDataToFitGraph(sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, ref trueLabels);
    }

    public void init()
    {
        labels = new float[10000, 4];
        trueLabels = new float[10000, 4];
        Rescaledlabels = new float[10000, 4];
        labelLength = 0;
        trueLabelLength = 0;
        labelIndex = 0;
        ifShowTrueLabels = toggle.isOn;

        string labelFilePath = getLabelFilePathByDayHour(sampleGraph.yearIndex, sampleGraph.monthIndex, sampleGraph.dayIndex);
        filePath.Clear();
        filePath.Add(labelFilePath);

        loadCsvLabelGrid(labelFilePath, ref labelIndex, ref trueLabelLength, ref trueLabels);
        scaleDataToFitGraph(sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, ref trueLabels);
    }
    public Canvas myCanvas;
    // Update is called once per frame
    private float offset = 30f;
    private int day;
    private float time;
    public bool canUpdatePointerPosition = true;

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (myCanvas)
            {
                Vector2 pos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);
                if (canUpdatePointerPosition && Mathf.Abs(pos.y) < 200)
                {
                    mousePos = pos;
                    updatePointerPosition();
                }
                Debug.Log(mousePos);
            }
        }

        // If get right mouse button, cancel the labelling
        if (Input.GetMouseButton(1))
        {
            if (myCanvas)
            {
                selectedLabelIndex = -1;
                DropDownLabel.Instance.Value = DropDownLabel.Instance.DefaultOptionValue;
            }
        }

        if (selectedLabelIndex >= 0)
        {
            updatePointerPosition();
            labels[selectedLabelIndex, 2] = time;
            Rescaledlabels[selectedLabelIndex, 2] = sampleGraph.graphLeftBorder + (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * time / (3600f * 24);
        }
    }

    void updatePointerPosition()
    {
        float xPos = mousePos.x / sampleGraph.scale - sampleGraph.offset;
        //get day
        day = Mathf.FloorToInt((xPos - sampleGraph.graphLeftBorder) / (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder));
        //get time
        time = (xPos - sampleGraph.graphLeftBorder) / (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * 3600 * 24;
        //		Debug.Log (time);
        if (time < 0 || time > 24 * 3600)
            time = -1;
        //		Debug.Log((xPos - sampleGraph.graphLeftBorder) /  (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * 3600 * 24);
        //		time = (xPos - sampleGraph.graphLeftBorder) % (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder);
        //		time = time / (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * 3600 * 24;
        //Debug.Log (time);
    }

    public int selectedLabelIndex = -1;

    public void addLabel(int type)
    {
        if (selectedLabelIndex >= 0)
        {
            if (Mathf.RoundToInt(labels[selectedLabelIndex, 3]) == type)
            {
                selectedLabelIndex = -1;
                return;
            }
            else
            {
                selectedLabelIndex = labelLength;
            }

        }
        else
        {
            selectedLabelIndex = labelLength;
        }

        updatePointerPosition();

        if (day < 0)
            return;
        if (time < 0)
            return;
        if (type >= Global.labelType.Count)
            return;

        int i = labelLength;
        labels[i, 0] = time;
        labels[i, 1] = time;
        labels[i, 2] = time;
        labels[i, 3] = type;

        float labelPosX = labels[i, 0] * (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) + sampleGraph.graphLeftBorder;
        labelPosX += labels[i, 1] / (3600 * 24) * (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder);
        labelPosX = (labelPosX + sampleGraph.offset) * sampleGraph.scale;

        labelLength++;

        for (int k = 0; k < labelLength; k++)
        {
            Rescaledlabels[k, 0] = sampleGraph.graphLeftBorder + (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * labels[k, 1] / (3600f * 24);
            Rescaledlabels[k, 1] = Rescaledlabels[k, 0];
            Rescaledlabels[k, 2] = sampleGraph.graphLeftBorder + (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * labels[k, 2] / (3600f * 24);
            Rescaledlabels[k, 3] = labels[k, 3];
        }

        Debug.Log(labels[0, 0]);

        RemovedLastLabel = false;
    }

    public void addLabel(float timeStamp, float startTime, float endTime, int type)
    {
        int k = labelLength;
        labelLength++;

        labels[k, 0] = timeStamp;
        labels[k, 1] = startTime;
        labels[k, 2] = endTime;
        labels[k, 3] = type;

        Rescaledlabels[k, 0] = sampleGraph.graphLeftBorder + (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * timeStamp / (3600f * 24);
        Rescaledlabels[k, 1] = Rescaledlabels[k, 0];
        Rescaledlabels[k, 2] = sampleGraph.graphLeftBorder + (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) * endTime / (3600f * 24);
        Rescaledlabels[k, 3] = type;

        RemovedLastLabel = false;
    }

    bool removedLastLabel = false;
    public void RemoveLastLabel()
    {
        if (RemovedLastLabel) return;
        int k = labelLength;
        labelLength--;

        labels[k, 0] = 0; Rescaledlabels[k, 0] = 0;
        labels[k, 1] = 0; Rescaledlabels[k, 1] = 0;
        labels[k, 2] = 0; Rescaledlabels[k, 2] = 0;
        labels[k, 3] = 0; Rescaledlabels[k, 3] = 0;

        RemovedLastLabel = true;
    }

    public void OnRenderObject()
    {
        float scale = sampleGraph.scale1, offset = sampleGraph.offset;
        CreateLineMaterial();
        // Apply the line material
        lineMaterial.SetPass(0);
        GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        GL.MultMatrix(transform.localToWorldMatrix);

        // Draw the line-graph based on the signal data
        GL.Begin(GL.LINES);

        GL.Color(new Color(1f, 0f, 0, 1F));

        //draw SignalLine
        GL.Vertex3(mousePos.x - 10, 70, 1);
        GL.Vertex3(mousePos.x + 10, 70, 1);
        GL.Vertex3(mousePos.x - 10, -132, 1);
        GL.Vertex3(mousePos.x + 10, -132, 1);

        GL.Vertex3(mousePos.x, 70, 1);
        GL.Vertex3(mousePos.x, -132, 1);




        //draw label
        //		for (int i = 0; i < labelLength; i++){
        //			float labelPosX = labels [i, 0] * (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder) + sampleGraph.graphLeftBorder;
        //			labelPosX += labels [i, 1] / (3600 * 24) * (sampleGraph.graphRightBorder - sampleGraph.graphLeftBorder);
        //
        //			int type = (int)labels [i, 2];
        //			if (type == 0)
        //				GL.Color(new Color(1f, 0f, 0, 1F));
        //			if (type == 1)
        //				GL.Color(new Color(0f, 1f, 0, 1F));
        //			if (type == 2)
        //				GL.Color(new Color(0f, 0f, 1, 1F));
        //			GL.Vertex3((labelPosX + offset) * scale, 130, 1);
        //			GL.Vertex3((labelPosX + offset) * scale, -130, 1);
        //
        //		}
        GL.End();

        //draw true labels
        if (ifShowTrueLabels)
        {
            for (int i = 0; i < trueLabelLength; i++)
            {
                float labelPosX = trueLabels[i, 0];
                //			Debug.Log ((labelPosX + offset) * scale);


                GL.Begin(GL.QUADS);

                Color color = Global.colorList[Mathf.RoundToInt(trueLabels[i, 3])];
                color.a = 0.9f;
                GL.Color(color);


                GL.Vertex3((trueLabels[i, 1] + offset) * scale, 110, 1);
                GL.Vertex3((trueLabels[i, 1] + offset) * scale, 120, 1);
                GL.Vertex3((trueLabels[i, 2] + offset) * scale, 120, 1);
                GL.Vertex3((trueLabels[i, 2] + offset) * scale, 110, 1);
                GL.End();

            }
        }

        float deltaRange = 200f / (float)Global.labelType.Count;

        //draw labels created by players
        for (int i = 0; i < labelLength; i++)
        {
            GL.Begin(GL.QUADS);
            int j = Mathf.RoundToInt(Rescaledlabels[i, 3]);
            Color color = Global.colorList[j];
            color.a = 0.3f;
            GL.Color(color);
            //			Debug.Log (Rescaledlabels [i, 0] + ", " + Rescaledlabels [i, 1] + ", " + Rescaledlabels [i, 2]);
            GL.Vertex3((Rescaledlabels[i, 1] + offset) * scale, -132 + j * deltaRange, 1);
            GL.Vertex3((Rescaledlabels[i, 1] + offset) * scale, -132 + (j + 1) * deltaRange, 1);
            GL.Vertex3((Rescaledlabels[i, 2] + offset) * scale, -132 + (j + 1) * deltaRange, 1);
            GL.Vertex3((Rescaledlabels[i, 2] + offset) * scale, -132 + j * deltaRange, 1);

            GL.End();
        }

        GL.PopMatrix();



    }



    static Material lineMaterial;

    public bool RemovedLastLabel
    {
        get
        {
            return removedLastLabel;
        }

        set
        {
            removedLastLabel = value;

        }
    }

    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    string getLabelFilePathByDayHour(int year, int month, int day)
    {
        string dayStr = day.ToString("00");
        string monthStr = month.ToString("00");
        string yearStr = year.ToString("0000");
        //return "data/" + dayStr + "/Label-2017-" + monthStr + "-" + dayStr + ".csv";  
        string path = string.Format("data/{0}/Label-{1}-{2}-{3}.csv", dayStr, yearStr, monthStr, dayStr);
        if (!System.IO.File.Exists(path)) DebugDisplay.Instance.LogFormat("Cannot find: {0}", path);
        return path;
    }

    void loadCsvLabelGrid(string filePath, ref int index, ref int length, ref float[,] dataGrid)
    {
        if (!File.Exists(filePath))
        {
            Debug.Log("Cannot find: " + filePath);
            return;
        }

        StreamReader reader = new StreamReader(File.OpenRead(filePath));

        //data
        string[] data;//tmp

        for (; !reader.EndOfStream && index < dataGrid.Length; index++)
        {

            data = reader.ReadLine().Split(',');

            dataGrid[index, 0] = float.Parse(data[0]);
            dataGrid[index, 1] = float.Parse(data[1]);
            dataGrid[index, 2] = float.Parse(data[2]);

            string label = data[3];
            //
            //			int i;

            //
            //			if (i == Global.labelTypeCount) {
            //				Global.labelType [i] = label;
            //				Global.labelTypeCount++;
            //				dataGrid [index, 3] = i;
            //			}
            if (!Global.labelType.Contains(label))
            {
                Global.labelType.Add(label);
            }

            for (int i = 0; i < Global.labelType.Count; i++)
            {
                if (label == Global.labelType[i])
                {
                    dataGrid[index, 3] = i;
                    break;
                }

            }

            //			 = (float)Global.labelType.fi
            length++;
        }

        reader.Close();
        return;

    }

    public void scaleDataToFitGraph(float leftBorder, float rightBorder, ref float[,] dataGrid)
    {
        //		Debug.Log (dataGrid [0, 0]);
        //		Debug.Log (dataGrid [24000, 0]);
        for (int i = 0; i < trueLabelLength; i++)
        {
            //					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
            dataGrid[i, 0] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 0] / (3600f * 24);
            dataGrid[i, 1] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 1] / (3600f * 24);
            dataGrid[i, 2] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 2] / (3600f * 24);
            //					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
        }
        //		Debug.Log (dataGrid [0, 0]);
        //		Debug.Log (dataGrid [24000, 0]);
    }

    public void saveTheLabel()
    {
        string fileName = "label/" + sampleGraph.monthIndex.ToString("D2") + "/" + sampleGraph.dayIndex.ToString("D2") + "/Label-" + sampleGraph.yearIndex.ToString("D4") + "-" + sampleGraph.monthIndex + "-" + sampleGraph.dayIndex + ".csv";
        if (File.Exists(fileName))
        {
            File.Delete(fileName);
        }
        Directory.CreateDirectory("label/" + sampleGraph.monthIndex.ToString("D2") + "/" + sampleGraph.dayIndex.ToString("D2"));
        StreamWriter sw = File.CreateText(fileName);
        Debug.Log("save file: " + fileName);
        string headers = "timestamp,start time, end time, label type";
        sw.WriteLine(headers);
        for (int i = 0; i < labelLength; i++)
        {
            int labelIndex = Mathf.RoundToInt(labels[i, 3]);
            string labelLine = labels[i, 0].ToString("F") + "," +
                labels[i, 1].ToString("F") + "," +
                labels[i, 2].ToString("F") + "," +
                Global.labelType[labelIndex];
            //				+ "," + labels [i, 1].ToString("F")  + "," + labels [i, 2].ToString("F")  + "," + Global.labelType [Mathf.RoundToInt(labels [i, 3])];
            sw.WriteLine(labelLine);
        }
        sw.Close();

    }

    public void saveLabelsOnFB()
    {
        firebase.writeLabels(labels, labelLength, 2017, sampleGraph.monthIndex, sampleGraph.dayIndex);
        Debug.Log("Upload labels successfully.");
    }

    public void retrieveLabelsFromFB()
    {
        firebase.getLabels(2017, sampleGraph.monthIndex, sampleGraph.dayIndex, this.GetComponent<RenderLabel>());
    }

    public void setLabels(float[,] labels, int length)
    {
        this.labels = labels;
        this.labelLength = length;
    }

    public void setTrueLabels(float[,] labels, int length)
    {
        this.trueLabels = labels;
        this.trueLabelLength = length;
    }

    public void initLabels()
    {
        labels = new float[10000, 4];
        labelLength = 0;
        Rescaledlabels = new float[10000, 4];
        labelIndex = 0;
    }

    public void toggleDisplayTrueLabels(bool flag)
    {
        ifShowTrueLabels = flag;
    }
}
