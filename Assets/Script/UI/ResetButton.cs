﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ResetButton : MonoBehaviour {
	public Slider slider;
	private float value = 0;
	private bool pauseFlag = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void resetValue(float value){
		slider.value = value;
	}

	public void PauseAndPlay(){
		if (pauseFlag == false) {
			pauseFlag = true;
			value = slider.value;
			slider.value = 0;
			this.GetComponentInChildren<Text>().text = "Play";
		} else {
			pauseFlag = false;
			slider.value = value;
			this.GetComponentInChildren<Text>().text = "Pause";
		}
	}
}
