﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LabelButton : MonoBehaviour {
	public int labelIndex = -1;
	private Text text;
	public RenderLabel labelGraph;
	// Use this for initialization
	void Start () {
		text =  this.GetComponentInChildren<Text> ();
		if (labelIndex >= Global.labelType.Count)
			return;
		text.text = Global.labelType [labelIndex];
		text.color = Color.black;
		this.GetComponent<Image>().color = Global.colorList [labelIndex];
	}
	
	// Update is called once per frame
	void Update () {
		if (labelIndex >= Global.labelType.Count) {
			text.text = "null";
			this.GetComponent<Button> ().interactable = false;
			return;
		} else
			this.GetComponent<Button> ().interactable = true;
		text.text = Global.labelType [labelIndex];
		Color color = Color.black;
		this.GetComponent<Image>().color = Global.colorList [labelIndex];
		color.a = 1;
		text.color = color;
	}

	public void addNewLabel(){
		Debug.Log (Global.colorList[labelIndex]);
		if (!labelGraph)
			return;
		if (labelIndex >= Global.labelType.Count || labelIndex < 0)
			return;
		labelGraph.addLabel (labelIndex);

	}
}
