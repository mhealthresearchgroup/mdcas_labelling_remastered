﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ButtonInit : MonoBehaviour {
	public GameObject obj;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < 31; i++) {
			Global.lenthOfDayData [i] = new int[24]{
				0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0,};
//			for (int j = 0; j < 24; j++) {
//				myJob [i, j] = null;
//			}
		}
	}


	public void click(){
		initTarget (obj);
		initInfo ();
	}

	void initTarget(GameObject obj){
		if (obj) {
			obj.transform.position = new Vector3 (0, 1f, 8f);
			obj.GetComponent<Rigidbody>().velocity = Vector3.zero;
			ModelController modelCtrl = obj.GetComponent<ModelController>();
			modelCtrl.current = 1;
			modelCtrl.stop = 1;
		}
	}

	void initInfo(){
		Global.addSystemInfo("Init Successfully.");
	}


	public Job[,] myJob = new Job[31,25];
	public LabelJob[,] myLabelJob = new LabelJob[31,25];

	SubsampledDataJob testSubsampledDataJob;
	public void loadData(){
//		Global.dataOfDayGrid = new float[25][,];
//		Global.dataOfWeekGrid = new float[31][][,];
//
//		Debug.Log ("Start loading.");
//		for (int i = Global.precomputedDay; i < Global.precomputedDay+1; i++) {
//			Global.dataOfWeekGrid [i] = new float[25][,];
//			for (int j = 0; j < 24; j++) {
//				Global.dataOfWeekGrid [i] [j] = new float[300000, 3];
//				Debug.Log (Global.makeDataPathByDateAndHour (i, j));
//				myJob[i,j] = new Job ();
//				myJob[i,j].hourIndex = j;
//				myJob [i,j].dayIndex = i;
//				myJob[i,j].Start();
//			}
//		}

		Global.subsampledDataOfDayGrid = new float[7][,];
		for (int i = 0; i < 7; i++) {
			Global.subsampledDataOfDayGrid [i] = new float[50000, 4];
		}

		testSubsampledDataJob = new SubsampledDataJob ();
		testSubsampledDataJob.path = "data/04/precomputed_04.csv";
		testSubsampledDataJob.dayIndex = 4;
		testSubsampledDataJob.Start ();







		//		for (int i = 0; i < 24; i++) {
		//			Global.dataOfDayGrid [i] = new float[300000, 3];
		//
		//			myJob[i] = new Job ();
		//			myJob[i].hourIndex = i;
		//			myJob[i].Start();
		//		}


	}

	LabelJob testLabelJob;
	public void loadLabel(){
//		Global.labelOfHourDay = new string[31][][,];
//		for (int i = Global.precomputedDay; i < Global.precomputedDay + 1; i++) {
//			Global.labelOfHourDay [i] = new string[25][,];
//			for (int j = 0; j < 24; j++) {
//				Global.labelOfHourDay [i] [j] = new string[10, 4];
//				Debug.Log (Global.makeLabelPathByDateAndHour (i, j));
//				myLabelJob[i,j] = new LabelJob ();
//				myLabelJob[i,j].hourIndex = j;
//				myLabelJob [i,j].dayIndex = i;
//				myLabelJob[i,j].Start();
//			}
//		}

		Global.labelOfDay = new float[7][,];
		for (int i = 0; i < 7; i++) {
			Global.labelOfDay [i] = new float[100, 4];
		}

		testLabelJob = new LabelJob ();
		testLabelJob.dayIndex = 4;


		testLabelJob.Start ();


	}

	// Update is called once per frame
	void Update () {
		if (testLabelJob != null){
			if (testLabelJob.Update ()) {
				testLabelJob = null;

//				Debug.Log (Global.labelOfDay [4] [0, 0] + "," + Global.labelOfDay [4] [0, 1] + "," + Global.labelOfDay [4] [0, 2] + "," + Global.labelOfDay [4] [0, 3]);
				float leftBorder = -400;
				float rightBorder = 400;
				for (int i = 0; i < Global.labelLengthOfDay [4]; i++) {
					Global.labelOfDay [4] [i, 0] = leftBorder + (rightBorder - leftBorder) * Global.labelOfDay [4] [i, 0] / (3600 * 24);
					Global.labelOfDay [4] [i, 1] = leftBorder + (rightBorder - leftBorder) * Global.labelOfDay [4] [i, 1] / (3600 * 24);
					Global.labelOfDay [4] [i, 2] = leftBorder + (rightBorder - leftBorder) * Global.labelOfDay [4] [i, 2] / (3600 * 24);

				}

//				Debug.Log (Global.labelOfDay [4] [24, 0] + "," + Global.labelOfDay [4] [24, 1] + "," + Global.labelOfDay [4] [24, 2] + "," + Global.labelOfDay [4] [24, 3]);
			}
		}
		if (testSubsampledDataJob!= null) {
			if (testSubsampledDataJob.Update ()) {
				testSubsampledDataJob = null;

				float leftBorder = -400;
				float rightBorder = 400;
				for (int i = 0; i < 28070; i++) {
//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
					Global.subsampledDataOfDayGrid [4] [i, 0] = leftBorder + (rightBorder - leftBorder) * Global.subsampledDataOfDayGrid [4] [i, 0] / 72000f;
					Global.subsampledDataOfDayGrid [4] [i, 1] = Global.subsampledDataOfDayGrid [4] [i, 1] * 100f;
//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
				}




				Debug.Log(Global.lenthOfDay[4] +  " ," + Global.subsampledDataOfDayGrid [4] [1, 0] + ", "+  Global.subsampledDataOfDayGrid [4] [1, 1]);

			}
		}
		for (int i = 6; i < 10; i++) {
			for (int j = 0; j < 24; j++) {
				if (myJob [i,j] != null) {
					if (myJob [i,j].Update ()) {
						myJob [i,j] = null;

					}
				}
			}
		}
		for (int i = 4; i < 10; i++) {
			for (int j = 0; j < 24; j++) {
				if (myLabelJob [i,j] != null) {
					if (myLabelJob [i,j].Update ()) {
						myLabelJob [i,j] = null;

					}
				}
			}
		}
	}
}