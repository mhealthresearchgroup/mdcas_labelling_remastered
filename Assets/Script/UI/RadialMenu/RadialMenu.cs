﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RadialMenu : MonoBehaviour
{

    public RadialButton buttonPrefab;
    public RadialButton selected;
    public RenderLabel labelGraph;
    public Button playPauseButton = null;

    // Use this for initialization
    public void spawnButtons(InteractionWithRadialMenu obj)
    {
        for (int i = 0; i < obj.options.Length; i++)
        {
            RadialButton newButton = Instantiate(buttonPrefab) as RadialButton;
            newButton.transform.SetParent(transform, false);

            float theta = i;
            float xPos = 0;
            float yPos = -theta;
            newButton.transform.localPosition = 40f * new Vector3(xPos, yPos, 0f);
            //float theta = (2 * Mathf.PI / obj.options.Length) * i;
            //float xPos = Mathf.Sin (theta);
            //float yPos = Mathf.Cos (theta);
            //newButton.transform.localPosition = 80f * new Vector3 (xPos, yPos, 0f);
            newButton.myMenu = this;
            newButton.circle.color = obj.options[i].color;
            newButton.icon.sprite = obj.options[i].sprite;
            newButton.labelTxt.text = obj.options[i].labelName;
            newButton.labelIndex = obj.options[i].labelIndex;
        }
    }

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!playPauseButton)
        {
            playPauseButton = GameObject.FindGameObjectWithTag("PlayPauseBtn").GetComponent<Button>();
            return;
        }

        if (Input.GetMouseButtonUp(1))
        {
            if (selected)
            {
                Debug.Log(selected.labelTxt.text += " was selected");
                if (selected.labelIndex == -2)   //Pause/Play button
                {
                        playPauseButton.onClick.Invoke();
                }
                else
                {
                    //labelGraph.addLabel(selected.labelIndex);
                    DropDownLabel.Instance.Value = selected.labelIndex;
                }
            }
            Destroy(gameObject);
        }
    }
}
