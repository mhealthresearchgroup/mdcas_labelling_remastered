﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionWithRadialMenu : MonoBehaviour {
	[System.Serializable]
	public class LabelButton{
		public Color color;
		public Sprite sprite;
		public string labelName;
		public int labelIndex;
	}

	public LabelButton[] options;
	public Sprite iconSprite;
    public Button playPauseButton = null;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!playPauseButton)
        {
            playPauseButton = GameObject.FindGameObjectWithTag("PlayPauseBtn").GetComponent<Button>();
            return;
        }

        if (Input.GetMouseButtonDown(1))
        {
            // set valid range of interaction
            if (myCanvas.ins)
            {
                Vector2 pos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(myCanvas.ins.transform as RectTransform, Input.mousePosition, myCanvas.ins.worldCamera, out pos);
                Debug.LogFormat("Pos y is: {0}", pos.y);
                if (pos.y < -100)
                {
                    return;
                }
                //				Debug.Log (mousePos);
            }
            // read label types
            int length = Global.labelType.Count;
            options = new LabelButton[length+1];
            Debug.Log(length);

            // pause/play button
            options[0] = new LabelButton();
            options[0].color = Color.gray;
            options[0].color.a = 0.8f;
            options[0].labelName = playPauseButton.GetComponentInChildren<Text>().text;
            options[0].sprite = iconSprite;
            options[0].labelIndex = -2;

            for (int i = 0; i < length; i++)
            {
                options[i+1] = new LabelButton();
                options[i+1].color = LabelColor.ins.colorList[i];
                options[i+1].color.a = 0.8f;
                options[i+1].labelName = Global.labelType[i];
                options[i+1].sprite = iconSprite;
                options[i+1].labelIndex = i;
            }

            //spawn menu
            RadialMenuSpawner.ins.SpawnMenu(this);
        }
    }



	void OnMouseDown(){
		Debug.Log ("?");
//		RadialMenuSpawner.ins.SpawnMenu (this);
	}
}
