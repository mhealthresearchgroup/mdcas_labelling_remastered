﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialMenuSpawner : MonoBehaviour {
	public static RadialMenuSpawner ins;
	public RadialMenu menuPrefab;
	public Canvas myCanvas;
	public RenderLabel labelGraph;

	void Awake(){
		ins = this;
	}

	public void SpawnMenu(InteractionWithRadialMenu obj){
		RadialMenu newMenu = Instantiate (menuPrefab) as RadialMenu;
		newMenu.transform.SetParent (transform, false);
		Vector2 pos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle (myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);
		newMenu.transform.localPosition = pos + new Vector2(0,-50);
		newMenu.labelGraph = labelGraph;
		newMenu.spawnButtons (obj);

	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
