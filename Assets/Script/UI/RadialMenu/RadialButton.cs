﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RadialButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler{

	public Image circle;
	public Image icon;
	public RadialMenu myMenu;
	public Text labelTxt;
	public int labelIndex;
	Color defaultColor;

	public void OnPointerEnter (PointerEventData eventData)
	{
		myMenu.selected = this;
		defaultColor = icon.color;
		Color tmp = defaultColor;
		tmp.a = 1f;
		icon.color = tmp;

	}
		
	public void OnPointerExit (PointerEventData eventData)
	{
		myMenu.selected = null;
		icon.color = defaultColor;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
