﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class DataController : MonoBehaviour {
	public TextAsset labelCsv;
	public TextAsset dataCsv;
	public TextAsset[] dayCsv = new TextAsset[24];
	private string[,] label;
	private string[,] data;
	public int startID, stopID;
	private Job myJob;
	private int num = 3;
	void Start () {
		Debug.Log ("Loading");
		Global.addSystemInfo ("Loading data...");

//		Global.labelGrid = CSVReader.SplitCsvGrid(labelCsv.text);
//		Global.dataGrid = CSVReader.SplitCsvGrid(dataCsv.text);
//		label = Global.labelGrid;
//		data = Global.dataGrid;
//		Global.addSystemInfo ("Load data successfully.");
//		Global.tabSystemInfo();

//		getDataByLabel (1); 
//		getDataByLabel (3); 
//		getDataByLabel (11); 
//		getDataByLabel (12); 
//		getDataByLabel (15);

//		for (int i = 0; i < 3; i++) {
//			Global.dayCsv [i] = dayCsv [i];
//		}
//





		//Debug.Log(Application.dataPath);

//		StartCoroutine (loadDataOfOneDay ());

	}

	// Update is called once per frame
	void Update () {
		if (myJob != null) {
			if (myJob.Update ()) {
				myJob = null;
			}
		}
	}

	IEnumerator loadDataOfOneDay(){
//		Global.dataOfDayGrid[i] = CSVReader.SplitCsvGrid(dayCsv[i].text);
//		Debug.Log (i + " ：Load data successfully.");
		yield return new WaitForSeconds(3);
		Debug.Log (Global.test);
	}


	void showLabel(int i){
		Debug.Log (label [0, i] + " " + label [1, i] + " " + label [2, i] + " " + label [3, i]);
		return;
	}

	public void getDataByLabel(int i){
		String labelName = label [3, i];
		DateTime startTime = getDate(label [1, i]);
		DateTime stopTime = getDate(label [2, i]);
		Global.addSystemInfo ("Duration: " + stopTime.Subtract (startTime).TotalSeconds + "s.");
		int start = 1, stop = 1;
		do {
			DateTime currentTime = getDate(data [0, stop]);
			//Debug.Log(currentTime);
			double temp;
			temp = startTime.Subtract (currentTime).TotalMilliseconds;
			//Debug.Log(temp);
			if (temp > 0)
				start++;
			temp = stopTime.Subtract (currentTime).TotalMilliseconds;
			//Debug.Log(temp);
			if (temp > 0)
				stop++;
			else {
				//Debug.Log(currentTime);
				break;
			}
		} while (true);
		startID = start;
		stopID = stop;
		Global.addSystemInfo("Label<" + labelName  + ">: (startId, stopId) = (" + start + ", " + stop + ").");
		Global.tabSystemInfo ();
	}

	DateTime getDate(string date){
		return DateTime.Parse(date);
	}
	


}