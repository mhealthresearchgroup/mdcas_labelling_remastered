using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 public class ThreadedJob
 {
     private bool m_IsDone = false;
     private object m_Handle = new object();
     private System.Threading.Thread m_Thread = null;
     public bool IsDone
     {
         get
         {
             bool tmp;
             lock (m_Handle)
             {
                 tmp = m_IsDone;
             }
             return tmp;
         }
         set
         {
             lock (m_Handle)
             {
                 m_IsDone = value;
             }
         }
     }
 
     public virtual void Start()
     {
         m_Thread = new System.Threading.Thread(Run);
         m_Thread.Start();
     }
     public virtual void Abort()
     {
         m_Thread.Abort();
     }
 
     protected virtual void ThreadFunction() { }
 
     protected virtual void OnFinished() { }
 
     public virtual bool Update()
     {
         if (IsDone)
         {
             OnFinished();
             return true;
         }
         return false;
     }
     public IEnumerator WaitFor()
     {
         while(!Update())
         {
             yield return null;
         }
     }
     private void Run()
     {
         ThreadFunction();
         IsDone = true;
     }
 }

public class Job : ThreadedJob
 {
     public Vector3[] InData;  // arbitary job data
     public Vector3[] OutData; // arbitary job data
	public int hourIndex;
	public int dayIndex;
 
     protected override void ThreadFunction()
     {
         // Do your threaded task. DON'T use the Unity API here
//		Global.dataOfDayGrid = new string[24][,];
		string path = "";
//		path = hourIndex.ToString("D2") + ".csv";
		path = Global.makeDataPathByDateAndHour(dayIndex, hourIndex);
		CSVReaderAsyn.SplitCsvGrid(path, hourIndex, dayIndex);

     }
     protected override void OnFinished()
     {
         // This is executed by the Unity main thread when the job is finished
		//Debug.Log(hourIndex + "finished");
		Debug.Log ("day " + dayIndex + " hour " + hourIndex + ": " + Global.lenthOfDayData [dayIndex][hourIndex]);
     }
 }

public class LabelJob : ThreadedJob
{
	public Vector3[] InData;  // arbitary job data
	public Vector3[] OutData; // arbitary job data
	public int hourIndex;
	public int dayIndex;
	public string path;

	protected override void ThreadFunction()
	{
//				Global.dataOfDayGrid = new string[24][,];
//				string path = "";
//				path = hourIndex.ToString("D2") + ".csv";
//				path = Global.makeLabelPathByDateAndHour(dayIndex, hourIndex);
		path = Global.makeLabelPathByDate(dayIndex);
		CSVReaderAsyn.SplitLabelCsvGrid(path,  dayIndex);

	}
	protected override void OnFinished()
	{
		// This is executed by the Unity main thread when the job is finished
		//Debug.Log(hourIndex + "finished");
		Debug.Log ("Completed loading data: " + path);
	}
}

public class SubsampledDataJob : ThreadedJob
{
	public Vector3[] InData;  // arbitary job data
	public Vector3[] OutData; // arbitary job data
	public int hourIndex;
	public int dayIndex;
	public string path;

	protected override void ThreadFunction()
	{
		// Do your threaded task. DON'T use the Unity API here
		//		Global.dataOfDayGrid = new string[24][,];
//		string path = "";
		//		path = hourIndex.ToString("D2") + ".csv";
//		path = Global.makeLabelPathByDateAndHour(dayIndex, hourIndex);
//		CSVReaderAsyn.SplitLabelCsvGrid(path, hourIndex, dayIndex);

		CSVReaderAsyn.SplitSubsampledCsvGridByPath (path, dayIndex);
		OnFinished ();

	}
	protected override void OnFinished()
	{
		// This is executed by the Unity main thread when the job is finished
		//Debug.Log(hourIndex + "finished");
		Debug.Log ("Completed loading data: " + path);
	}
}

public class loadDataThread : ThreadedJob
{
	public Vector3[] InData;  // arbitary job data
	public Vector3[] OutData; // arbitary job data
	public int hourIndex, dayIndex;
	public List<string> pathList;
	public float[,] dataGrid = new float[300000, 4];
	public int dataLength = 0, index = 0;
	public float graphLeftBorder = -400f, graphRightBorder = 400f, yScale = 100f;

	protected override void ThreadFunction()
	{
		// Do your threaded task. DON'T use the Unity API here
		//		Global.dataOfDayGrid = new string[24][,];
		//		string path = "";
		//		path = hourIndex.ToString("D2") + ".csv";
		//		path = Global.makeLabelPathByDateAndHour(dayIndex, hourIndex);
		//		CSVReaderAsyn.SplitLabelCsvGrid(path, hourIndex, dayIndex);
		if (pathList.Count == 0) {
			Debug.Log ("No file.");
			return;
		}

		Init ();

		foreach (string path in pathList) {
			if (System.IO.File.Exists (path)) {
				CSVReaderAsyn.loadCsvGrid (path, ref index, ref dataLength, ref dataGrid);
				//////Debug.Log ("Completed loading data: " + path);
                //DebugDisplay.LogFormatStatic("Completed loading data: {0}", path);
            }
            else {
				Debug.Log ("Cannot found: " + path);
                //DebugDisplay.LogFormatStatic("Cannot find: {0}", path);
//				Abort ();
			}
		}
		scaleDataToFitGraph (graphLeftBorder, graphRightBorder, yScale);
		OnFinished ();

	}
	protected void Init(){
		dataGrid = new float[300000, 4];
		dataLength = 0;
		index = 0;
	}

	protected override void OnFinished()
	{
		// This is executed by the Unity main thread when the job is finished
		//Debug.Log(hourIndex + "finished");
		//////Debug.Log ("Length of data: " + dataLength);

	}

	public void scaleDataToFitGraph(float leftBorder, float rightBorder, float yScale){
//		Debug.Log (dataGrid [0, 0]);
//		Debug.Log (dataGrid [24000, 0]);
		for (int i = 0; i < dataLength; i++) {
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
			dataGrid [i, 0] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 0] / (3600f * 24);
			dataGrid [i, 1] = dataGrid[i, 1] * yScale;
			dataGrid [i, 2] = dataGrid[i, 2] * yScale;
			dataGrid [i, 3] = dataGrid[i, 3] * yScale;
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
		}
//		Debug.Log (dataGrid [0, 0]);
//		Debug.Log (dataGrid [24000, 0]);
	}
		
}

public class loadLabelThread : ThreadedJob
{
	public Vector3[] InData;  // arbitary job data
	public Vector3[] OutData; // arbitary job data
	public int hourIndex, dayIndex;
	public List<string> pathList;
	public float[,] dataGrid = new float[10000, 4];
	public int dataLength = 0, index = 0;
	public float graphLeftBorder = -400f, graphRightBorder = 400f, yScale = 100f;

	protected override void ThreadFunction()
	{
		// Do your threaded task. DON'T use the Unity API here
		//		Global.dataOfDayGrid = new string[24][,];
		//		string path = "";
		//		path = hourIndex.ToString("D2") + ".csv";
		//		path = Global.makeLabelPathByDateAndHour(dayIndex, hourIndex);
		//		CSVReaderAsyn.SplitLabelCsvGrid(path, hourIndex, dayIndex);
		if (pathList.Count == 0) {
			Debug.Log ("No file.");
			return;
		}

		Init ();

		foreach (string path in pathList) {
			if (System.IO.File.Exists (path)) {
				CSVReaderAsyn.loadCsvLabelGrid (path, ref index, ref dataLength, ref dataGrid);
				Debug.Log ("Completed loading label: " + path);
			} else {
				Debug.Log ("Cannot found label file: " + path);
				//				Abort ();
			}
		}
		scaleDataToFitGraph (graphLeftBorder, graphRightBorder, yScale);
		OnFinished ();

	}
	protected void Init(){
		dataGrid = new float[10000, 4];
		dataLength = 0;
		index = 0;
	}

	protected override void OnFinished()
	{
		// This is executed by the Unity main thread when the job is finished
		//Debug.Log(hourIndex + "finished");
		Debug.Log ("Length of data: " + dataLength);

	}

	public void scaleDataToFitGraph(float leftBorder, float rightBorder, float yScale){
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
		for (int i = 0; i < dataLength; i++) {
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
			dataGrid [i, 0] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 0] / (3600f * 24);
			dataGrid [i, 1] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 1] / (3600f * 24);
			dataGrid [i, 2] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 2] / (3600f * 24);
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
		}
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
	}

}

public class loadActivityCountsThread : ThreadedJob
{
	public Vector3[] InData;  // arbitary job data
	public Vector3[] OutData; // arbitary job data
	public int hourIndex, dayIndex;
	public List<string> pathList;
	private static int fileLength = 50000;
	public float[,] dataGrid = new float[fileLength, 2];
	public int dataLength = 0, index = 0;
	public float graphLeftBorder = -400f, graphRightBorder = 400f, yScale = 100f;

	protected override void ThreadFunction()
	{
		// Do your threaded task. DON'T use the Unity API here
		//		Global.dataOfDayGrid = new string[24][,];
		//		string path = "";
		//		path = hourIndex.ToString("D2") + ".csv";
		//		path = Global.makeLabelPathByDateAndHour(dayIndex, hourIndex);
		//		CSVReaderAsyn.SplitLabelCsvGrid(path, hourIndex, dayIndex);
		if (pathList.Count == 0) {
			Debug.Log ("No file.");
			return;
		}

		Init ();

		foreach (string path in pathList) {
			if (System.IO.File.Exists (path)) {
				CSVReaderAsyn.loadCsvActivityCountsGrid (path, ref index, ref dataLength, ref dataGrid);
				Debug.Log ("Completed loading label: " + path);
			} else {
				Debug.Log ("Cannot found label file: " + path);
				//				Abort ();
			}
		}
		scaleDataToFitGraph (graphLeftBorder, graphRightBorder, yScale);
		OnFinished ();

	}
	protected void Init(){
		dataGrid = new float[fileLength, 2];
		dataLength = 0;
		index = 0;
	}

	protected override void OnFinished()
	{
		// This is executed by the Unity main thread when the job is finished
		//Debug.Log(hourIndex + "finished");
		Debug.Log ("Length of data: " + dataLength);

	}

	public void scaleDataToFitGraph(float leftBorder, float rightBorder, float yScale){
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
		for (int i = 0; i < dataLength; i++) {
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
			dataGrid [i, 0] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 0] / (3600f * 24);
			dataGrid [i, 1] = dataGrid [i, 1] * yScale;
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
		}
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
	}

}

