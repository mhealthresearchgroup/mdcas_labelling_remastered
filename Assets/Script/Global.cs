﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public enum GameStatement {preparing, loading, playing, ending};
public enum NetworkStatement {disconnected, connected};
public static class Global{
	public static string test;

	public static string[,] dataGrid;
	public static string[,] labelGrid;

	public static float[][,] dataOfDayGrid;
	public static float[][][,] dataOfWeekGrid;
	public static float[][,] subsampledDataOfDayGrid;

	public static int[][] lenthOfDayData = new int[31][];
	public static int[] lenthOfDay = new int[7];
	public static float[][,] labelOfDay;
	public static string[][][,] labelOfHourDay;
	public static int[] labelLengthOfDay = new int[7];

//	public static string[] labelType;
//	public static int labelTypeCount = 0;
	public static List<string> labelType = new List<string>();

	public static TextAsset[] dayCsv = new TextAsset[25];
	public static List<Label> labelList = new List<Label>();
	public static List<Vector2> list_x = new List<Vector2>(), list_y = new List<Vector2>(), list_z = new List<Vector2>();
	public static List<Color> colorList = new List<Color>{
		new Color(0f, 0.71f, 0.8f, 0.7f), 
		new Color(0.96f, 0.57f, 0.34f, 0.7f), 
		new Color(0.91f, 0.85f, 0.55f, 0.7f), 
		new Color(0.89f, 0.53f, 0.56f, 0.7f), 
		new Color(0.5f, 0.82f, 0.71f, 0.7f), 
		new Color(0.82f, 0.68f, 0.61f, 0.7f),
		new Color(0.11f, 0.42f, 0.54f, 0.7f),
		new Color(1f, 0f, 0f, 0.7f),
		new Color(0.5f, 0.5f, 0.5f, 0.7f)
	};
//	public static Dictionary<string, int> labelStrToIntDic = Dictionary<string, int> ();

	public static string username = "guest";
//	public static Dictionary<int, Color> labelIntToColorDic = Dictionary<int, Color>();

	public static string[] label = { "walking", "jumping", "sitting" };
	public static string systemInfo = "";
	public static string addSystemInfo(string newInfo){
		string newStr = Global.systemInfo + newInfo + "\n";
		Global.systemInfo = newStr;
		GameObject obj = GameObject.Find("SystemInfo");
		if (obj)
			obj.GetComponent<Text> ().text = newStr;
		return newStr;
		//		globalVariables.systemInfo.

	}

	public static int precomputedDay = 4;

	public static void tabSystemInfo(){
		string newStr = Global.systemInfo + "---------------------------------------" + "\n";
		Global.systemInfo = newStr;
		GameObject obj = GameObject.Find("SystemInfo");
		if (obj)
			obj.GetComponent<Text> ().text = newStr;
		return;
	}

	public static void clearSystemInfo(){
		string newStr = "";
		Global.systemInfo = newStr;
		GameObject obj = GameObject.Find("SystemInfo");
		if (obj)
			obj.GetComponent<Text> ().text = newStr;
		return;
	}
		
	public static Firebase.Storage.FirebaseStorage storage = Firebase.Storage.FirebaseStorage.DefaultInstance;
	public static Firebase.Storage.StorageReference storage_ref = Global.storage.GetReferenceFromUrl ("gs://testbase-6e87a.appspot.com");
	public static Firebase.Storage.StorageReference csv_ref = Global.storage_ref.Child ("csv");
//
	public static string getDataPathByDateAndHour(int date, int hour){
		return "data/" + date.ToString("D2") + "/" + hour.ToString("D2") 
			+ "/ActigraphGT3XPLUS-AccelerationCalibrated-NA.CLE2B41120122-AccelerationCalibrated.2013-07-" 
			+ date.ToString("D2") + "-" + hour.ToString("D2") 
			+ "-00-00-000-M0400.sensor.csv.gz";
	}

	public static string getLabelPathByDateAndHour(int date, int hour){
		return "data/" + date.ToString("D2") + "/" + hour.ToString("D2") + "/FreeLiving.algo-FreeLiving.2013-07-"
			+ date.ToString("D2") + "-" + hour.ToString("D2") 
			+ "-00-00-000-M0500.annotation.csv.gz";
	}




	public static string makeDataPathByDateAndHour(int date, int hour){
		return  "data/" + date.ToString("D2") + "/" + hour.ToString("D2") 
			+ "/Data-"
			+ date.ToString("D2") + "-" + hour.ToString("D2") 
			+ ".csv";
	}


	public static string makeLabelPathByDateAndHour(int date, int hour){
		return  "data/" + date.ToString("D2") + "/" + hour.ToString("D2") 
			+ "/Label-"
			+ date.ToString("D2") + "-" + hour.ToString("D2") 
			+ ".csv";
	}

	public static string makeLabelPathByDate(int date){
		return  "data/" + date.ToString("D2")
			+ "/label_"
			+ date.ToString("D2")
			+ ".csv";
	}

	public static float getLabelCode(string labelName){
		switch (labelName) {
		case "Non-Wear": 
			return 0;
		case "Wear":
			return 1;
		default:
			return -1;
		}
	}


}

