﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ActivityCountGraph : MonoBehaviour {
	private loadLabelThread labelThread = new loadLabelThread();
	public List<string> filePath;
	public RenderGraphOnObject sampleGraph;
	private float[,] activityCounts = new float[50000, 2];
	private int activityCountsLength = 0;
	private int activityCountsIndex = 0;
	public bool ifShowActivityCounts = false;
	public float yscale = 1f;
	public float yZeroLine = 150f;

	// Use this for initialization
	void Start () {
		string ActivityFilePath = getActivityCountsFilePathByDay (sampleGraph.monthIndex, sampleGraph.dayIndex);
		filePath.Clear ();
		filePath.Add (ActivityFilePath);
		loadCsvActivityCountsGrid (ActivityFilePath, ref activityCountsIndex, ref activityCountsLength, ref activityCounts);
		scaleActivityCountsToFitGraph (sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, yscale,activityCountsLength, ref activityCounts);

	}

	public void init(){
	
		loadLabelThread labelThread = new loadLabelThread();
		activityCounts = new float[50000, 2];
		activityCountsLength = 0;
		activityCountsIndex = 0;
		ifShowActivityCounts = false;

		string ActivityFilePath = getActivityCountsFilePathByDay (sampleGraph.monthIndex, sampleGraph.dayIndex);
		filePath.Clear ();
		filePath.Add (ActivityFilePath);
		loadCsvActivityCountsGrid (ActivityFilePath, ref activityCountsIndex, ref activityCountsLength, ref activityCounts);
		scaleActivityCountsToFitGraph (sampleGraph.graphLeftBorder, sampleGraph.graphRightBorder, yscale,activityCountsLength, ref activityCounts);

	}

	// Update is called once per frame
	void Update () {
		
	}


	static Material lineMaterial;
	static void CreateLineMaterial()
	{
		if (!lineMaterial)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things.
			Shader shader = Shader.Find("Hidden/Internal-Colored");
			lineMaterial = new Material(shader);
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			// Turn on alpha blending
			lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			// Turn backface culling off
			lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			// Turn off depth writes
			lineMaterial.SetInt("_ZWrite", 0);
		}
	}

	int renderBorder = 800;

	public void OnRenderObject()
	{
		CreateLineMaterial();
		// Apply the line material
		lineMaterial.SetPass(0);

		GL.PushMatrix();
		// Set transformation matrix for drawing to
		// match our transform
		GL.MultMatrix(transform.localToWorldMatrix);

		// Draw the line-graph based on the signal data
		GL.Begin(GL.LINES);

		float offset = sampleGraph.offset, scale1 = sampleGraph.scale1;

		for (int i = 0; i < activityCountsLength-1; i++) {
			float x1 = activityCounts [i, 0];
			float y1 = activityCounts [i, 1];

			float x2 = activityCounts [i+1, 0];
			float y2 = activityCounts [i+1, 1];

			GL.Color(new Color(0.37f, 0.61f, 0.84f, 1F));

			GL.Vertex3((x1 + offset) * scale1, y1 + yZeroLine, 1);
			GL.Vertex3((x2 + offset) * scale1, y2 + yZeroLine, 1);
		}

		//draw coordinate
		GL.Color(new Color(0.5f, 0.5f, 0.5f, 0.5F));

		for (int i = 0; i <= 5; i++){
			GL.Vertex3((sampleGraph.graphLeftBorder + offset) * scale1, i * yscale + yZeroLine, 1);
			GL.Vertex3((sampleGraph.graphRightBorder + offset) * scale1, i * yscale  + yZeroLine, 1);
		}



		GL.End();
		GL.PopMatrix();
	}



	string getActivityCountsFilePathByDay(int month, int day){
		string dayStr = day.ToString("00");
		string monthStr = month.ToString("00");
		string path =  "data/" + dayStr + "/ActigraphGT9X-AccelerationCalibrated-NA.TAS1E23150052-AccelerationCalibrated.2017-"
            + monthStr + "-" + dayStr + "-M0400.counts.csv";  
		return path;
	}

	public void loadCsvActivityCountsGrid(string filePath, ref int index, ref int length,ref float[,] dataGrid){
		if (!File.Exists (filePath)) {
			Debug.Log ("Cannot find: " + filePath);
			return;
		}
		
		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 


		//data
		string[] data;//tmp

		for (; !reader.EndOfStream && index < dataGrid.Length; index++) {

			data = reader.ReadLine ().Split (',');

			dataGrid[index, 0] = float.Parse (data [0]);
			dataGrid[index, 1] = float.Parse (data [1]);

			length++;
		}

		reader.Close ();
		return;
	}

	public void scaleActivityCountsToFitGraph(float leftBorder, float rightBorder, float yScale, int dataLength, ref float[,] dataGrid){
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
		for (int i = 0; i < dataLength; i++) {
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
			dataGrid [i, 0] = leftBorder + (rightBorder - leftBorder) * dataGrid[i, 0] / (3600f * 24);
			dataGrid [i, 1] = dataGrid [i, 1] * yScale;
			//					Debug.Log (Global.subsampledDataOfDayGrid [4] [i, 0] + "," + Global.subsampledDataOfDayGrid [4] [i, 1]);
		}
		//		Debug.Log (dataGrid [0, 0]);
		//		Debug.Log (dataGrid [24000, 0]);
	}
}
