﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	private Ray rayRotate = new Ray (Vector3.zero, new Vector3 (0, 1, 0));
	private Ray rayTranslate = new Ray (Vector3.zero, new Vector3(0, 1, 0));
	private RaycastHit hitInfo;

	private float preTime = 0.0f;

	private bool flag = false;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		Vector3 direction = Vector3.zero;
		float px, pz;
		Camera camera = Camera.main; 

		if (Input.GetMouseButtonDown (0)) {
			
			rayTranslate = camera.ScreenPointToRay (Input.mousePosition);

			Physics.Raycast (camera.transform.position, rayRotate.direction, hitInfo.distance);
			if (Time.time - preTime < 0.3f) {
				Vector3 temp = new Vector3 (0, 5, 0);
				hitInfo.point = hitInfo.point + temp;
				transform.position = Vector3.MoveTowards (camera.transform.position, hitInfo.point, 10.0f);
			}

			preTime = Time.time;
		}

		//drag and move
		if (Input.GetMouseButton(0)){
			px = Input.GetAxis ("Mouse X");
			pz = Input.GetAxis ("Mouse Y");
			direction = new Vector3 (px, pz, pz);
			transform.Translate (-1 * direction * 30 * Time.deltaTime);
		}

		//large and small
		pz = Input.GetAxis("Mouse ScrollWheel");
		direction = new Vector3(0, 0, pz);
		transform.Translate(direction * 100 * Time.deltaTime);

		//rotate
		if(Input.GetMouseButtonDown(1)){
			rayRotate = camera.ScreenPointToRay(Input.mousePosition);
			flag = true ;
		}

		if(Input.GetMouseButtonUp(1)){
			rayRotate = new Ray(Vector3.zero, new Vector3(0,1,0));
			flag = false ;
		}

		if(Input.GetMouseButton(1) && flag){
			Physics.Raycast(camera.transform.position, rayRotate.direction, hitInfo.distance);
			px = Input.GetAxis("Mouse X") ;
			transform.RotateAround(hitInfo.point, Vector3.up, 100 * px * Time.deltaTime);
		}



	}
}
