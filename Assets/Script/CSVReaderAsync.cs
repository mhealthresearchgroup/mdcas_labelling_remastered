﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

public class CSVReaderAsyn {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	static public void SplitCsvGrid(string filePath, int hourIndex, int dayIndex){
		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 
		//head
		reader.ReadLine();

		//data
		string tmp;
		string[] data;
		Global.lenthOfDayData [dayIndex][hourIndex] = 0;

//		for (int i = 0; !reader.EndOfStream; i++) {
//			data = reader.ReadLine ().Split (',');
		//			Global.dataOfDayGrid [hourIndex] [i, 0] = float.Parse (data [1]);
		//			Global.dataOfDayGrid [hourIndex] [i, 1] = float.Parse (data [2]);
		//			Global.dataOfDayGrid [hourIndex] [i, 2] = float.Parse (data [3]);
//
//			Global.lenthOfDayData [dayIndex]++;
//		}

		for (int i = 0; !reader.EndOfStream; i++) {
			data = reader.ReadLine ().Split (',');

			Global.dataOfWeekGrid[dayIndex][hourIndex][i, 0] = float.Parse (data [1]);
			Global.dataOfWeekGrid[dayIndex][hourIndex][i, 1] = float.Parse (data [2]);
			Global.dataOfWeekGrid[dayIndex][hourIndex][i, 2] = float.Parse (data [3]);
		

			Global.lenthOfDayData [dayIndex][hourIndex]++;
		}
		reader.Close ();
		return;
	}
		

	static public void SplitLabelCsvGrid(string filePath, int dayIndex){
		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 
//		//head
//		reader.ReadLine();

		//data
		string tmp;
		string[] data;
		//Global.lenthOfDayData [dayIndex][hourIndex] = 0;

		//		for (int i = 0; !reader.EndOfStream; i++) {
		//			data = reader.ReadLine ().Split (',');
		//			Global.dataOfDayGrid [hourIndex] [i, 0] = float.Parse (data [1]);
		//			Global.dataOfDayGrid [hourIndex] [i, 1] = float.Parse (data [2]);
		//			Global.dataOfDayGrid [hourIndex] [i, 2] = float.Parse (data [3]);
		//
		//			Global.lenthOfDayData [dayIndex]++;
		//		}

		Global.labelLengthOfDay [dayIndex] = 0; 
		for (int i = 0; !reader.EndOfStream; i++) {
			data = reader.ReadLine ().Split (',');

			Global.labelOfDay[dayIndex][i, 0] = float.Parse (data [0]);
			Global.labelOfDay[dayIndex][i, 1] = float.Parse (data [1]);
			Global.labelOfDay[dayIndex][i, 2] = float.Parse (data [2]);
			Global.labelOfDay[dayIndex][i, 3] = Global.getLabelCode(data [3]);


			//Global.lenthOfDayData [dayIndex][hourIndex]++;
			Global.labelLengthOfDay [dayIndex]++;
		}
		return;
	}

	static public void SplitSubsampledCsvGridByPath(string filePath, int dayIndex){
		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 

		//data
		string tmp;
		string[] data;


		int lengthOfDay = 0;

		//		for (int i = 0; !reader.EndOfStream; i++) {
		//			data = reader.ReadLine ().Split (',');
		//			Global.dataOfDayGrid [hourIndex] [i, 0] = float.Parse (data [1]);
		//			Global.dataOfDayGrid [hourIndex] [i, 1] = float.Parse (data [2]);
		//			Global.dataOfDayGrid [hourIndex] [i, 2] = float.Parse (data [3]);
		//
		//			Global.lenthOfDayData [dayIndex]++;
		//		}

		for (int i = 0; !reader.EndOfStream; i++) {
			data = reader.ReadLine ().Split (',');

			Global.subsampledDataOfDayGrid[dayIndex][i, 0] = float.Parse (data [0]);
			Global.subsampledDataOfDayGrid[dayIndex][i, 1] = float.Parse (data [1]);
			Global.subsampledDataOfDayGrid[dayIndex][i, 2] = float.Parse (data [2]);
			Global.subsampledDataOfDayGrid[dayIndex][i, 3] = float.Parse (data [3]);


			lengthOfDay++;
		}
		Global.lenthOfDay [dayIndex] = lengthOfDay;
		reader.Close ();
		return;
	}

	//return array
	static public void loadCsvGrid(string filePath, ref int index, ref int length,ref float[,] dataGrid){
		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 

		//data
		string[] data;//tmp

		for (; !reader.EndOfStream && index < dataGrid.Length; index++) {
			
			data = reader.ReadLine ().Split (',');

			dataGrid[index, 0] = float.Parse (data [0]);
			dataGrid[index, 1] = float.Parse (data [1]);
			dataGrid[index, 2] = float.Parse (data [2]);
			dataGrid[index, 3] = float.Parse (data [3]);


			length++;
		}

		reader.Close ();
		return;
	}


	static public void loadCsvLabelGrid(string filePath, ref int index, ref int length,ref float[,] dataGrid){
		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 

		//data
		string[] data;//tmp

		for (; !reader.EndOfStream && index < dataGrid.Length; index++) {

			data = reader.ReadLine ().Split (',');

			dataGrid[index, 0] = float.Parse (data [0]);
			dataGrid[index, 1] = float.Parse (data [1]);
			dataGrid[index, 2] = float.Parse (data [2]);

			string label = data [3];

//			int i;
//			for (i = 0; i < Global.labelTypeCount; i++) {
//				if (label == Global.labelType[i])
//					dataGrid [index, 3] = i;
//			}
//
//			if (i == Global.labelTypeCount) {
//				Global.labelType [i] = label;
//				Global.labelTypeCount++;
//				dataGrid [index, 3] = i;
//			}

//			 = (float)Global.labelType.fi
			length++;
		}

		reader.Close ();
		return;
	
	}

	static public void loadCsvActivityCountsGrid(string filePath, ref int index, ref int length,ref float[,] dataGrid){
		StreamReader reader = new StreamReader (File.OpenRead (filePath)); 

		//data
		string[] data;//tmp

		for (; !reader.EndOfStream && index < dataGrid.Length; index++) {

			data = reader.ReadLine ().Split (',');

			dataGrid[index, 0] = float.Parse (data [0]);
			dataGrid[index, 1] = float.Parse (data [1]);

			length++;
		}

		reader.Close ();
		return;
	}
}
