﻿using UnityEngine;
using UnityEngine.UI;
public class TestGraph : MonoBehaviour
{
	// When added to an object, draws colored rays from the
	// transform position.
	public int lineCount = 100;
	public float radius = 3.0f;

	static Material lineMaterial;
	static void CreateLineMaterial()
	{
		if (!lineMaterial)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things.
			Shader shader = Shader.Find("Hidden/Internal-Colored");
			lineMaterial = new Material(shader);
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			// Turn on alpha blending
			lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			// Turn backface culling off
			lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			// Turn off depth writes
			lineMaterial.SetInt("_ZWrite", 0);
		}
	}

	private float offset = 0, scale = 1, scale1 = 1;
	// Will be called after all regular rendering is done


	void Update(){
		if (Input.GetAxis ("Mouse ScrollWheel") != 0) {
			if (Input.GetKey (KeyCode.Q)) {
				scale1 *= 1 + Input.GetAxis ("Mouse ScrollWheel");
				Vector3 scaleVtr = this.gameObject.GetComponent<RectTransform> ().localScale;
				scaleVtr.x = scale1;
				this.gameObject.GetComponent<RectTransform> ().localScale = scaleVtr;

				Debug.Log (scale1);
			}
			if (Input.GetKey (KeyCode.E)) {
				offset += scale * 100 * Input.GetAxis ("Mouse ScrollWheel");
			}

//			if (Input.GetKeyDown(KeyCode.W))
//				offset *= 1 + Input.GetAxis ("Mouse ScrollWheel");
		}
		if (Input.GetKeyDown (KeyCode.R)) {
			offset = 0;
		}



	}

	public void OnRenderObject()
	{
		if (Global.lenthOfDay [4] < 10)
			return;
		float leftBorder = -100;
		float rightBorder = 100;
		float delta = 200f / Global.lenthOfDay[4];
//		Debug.Log ("delta: " + delta);


		CreateLineMaterial();
		// Apply the line material
		lineMaterial.SetPass(0);

		GL.PushMatrix();
		// Set transformation matrix for drawing to
		// match our transform
		GL.MultMatrix(transform.localToWorldMatrix);

		// Draw lines
		GL.Begin(GL.LINES);

//		for (float i = -100; i < rightBorder; i += delta)
//		{
//			float a = Mathf.Abs(i / (float)rightBorder);
//
//
//			GL.Color(new Color(a, 1 - a, 0, 0.8F));
//			// One vertex at transform position
//			GL.Vertex3(i, 0, 0);
//			// Another vertex at edge of circle
//			GL.Vertex3(i, 100, 0);
//		}

		for (int i = 0; i < 28070 - 1; i++) {
			float x1 = Global.subsampledDataOfDayGrid[4][i, 0];
			float y1 = Global.subsampledDataOfDayGrid [4] [i, 1];

			float x2 = Global.subsampledDataOfDayGrid[4][i+1, 0] ;
			float y2 = Global.subsampledDataOfDayGrid [4] [i+1, 1];

			GL.Color(new Color(0.5f, 0.5f, 0, 0.8F));
			// One vertex at transform position
			GL.Vertex3(x1* scale + offset, y1, 0);
			// Another vertex at edge of circle
			GL.Vertex3(x2* scale + offset, y2, 0);

		}

//		float labelY;
//		for (int i = 0; i < 24; i++) {
//			if (i % 2 == 0){
//				GL.Color(new Color(1f, 0f, 0, 0.8F));
//				labelY = -120;
//			}
//			else{
//				GL.Color(new Color(0f, 0f, 1, 0.8F));
//				labelY = -150;
//			}
//			
//			GL.Vertex3((-400 + 800 * i / 24) * scale + offset , labelY, 0);
//			GL.Vertex3((-400 + 800 * (i+1) / 24) * scale + offset, labelY, 0);
//		}

		if (Global.labelLengthOfDay [4] < 10)
			return;

		for (int i = 0; i < Global.labelLengthOfDay [4]; i++) {
			float x1 = Global.labelOfDay [4] [i, 1] * scale + offset;
			float x2 = Global.labelOfDay [4] [i, 2] * scale + offset;
			float y = -100;
			if (i % 2 == 0)
				y = -130;
			float color = Global.labelOfDay [4] [i, 3];
			GL.Color (new Color (color, 1 - color, 1, 1));
			GL.Vertex3 (x1, y, 0);
			GL.Vertex3 (x2, y, 0);


		}




		// coordinate
		GL.Color(new Color(0f, 0f, 1, 0.8F));

		GL.Vertex3(-400 * scale + offset, 0, 0);
		GL.Vertex3(400 * scale + offset, 0, 0);

		float deltaRange = 800.0f / (6f * 24f); 

		for (int i = 0; i < 24; i++) {
			for (int j = 0; j < 6; j++) {
				float x = -400 + (i * 6 + j) * deltaRange;
				x = x * scale + offset;
				if (j == 0) {
					GL.Vertex3 (x, 10, 0);
					GL.Vertex3 (x, -10, 0);
				} else {
					if (scale1 > 2) {
						GL.Vertex3 (x, 7, 0);
						GL.Vertex3 (x, -7, 0);
					}
				}
			}
		}

		GL.Color(new Color(0f, 0f, 1f, 0.8F));

		deltaRange = 800.0f / (6f * 24f * 5f); 
		if (scale1 > 15) {
			for (int i = 0; i < 24; i++) {
				for (int j = 0; j < 6; j++) {
					for (int k = 1; k < 5; k ++) {
						float x = -400 + (i * 6 * 5 + j * 5 + k ) * deltaRange;
						x = x * scale + offset;
						GL.Vertex3 (x, 5, 0);
						GL.Vertex3 (x, -5, 0);
					}
				}
			}
		}


		GL.End();


		GL.Begin(GL.QUADS);

		for (int i = 0; i < Global.labelLengthOfDay [4]; i++) {
			float x1 = Global.labelOfDay [4] [i, 1] * scale + offset;
			float x2 = Global.labelOfDay [4] [i, 2] * scale + offset;
			float y1 = -100;
			float y2 = 100;
			float color = Global.labelOfDay [4] [i, 3];
			GL.Color (new Color (color, 1 - color, 1, 0.1f));
			GL.Vertex3 (x1, y1, 0);
			GL.Vertex3 (x1, y2, 0);
			GL.Vertex3 (x2, y2, 0);
			GL.Vertex3 (x2, y1, 0);

		}
			

		GL.End ();

		GL.PopMatrix();

	}
}