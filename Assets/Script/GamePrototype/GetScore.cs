﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetScore : MonoBehaviour {
	public GameObject labelList;
	private LabelTest[] list;
	// Use this for initialization
	void Start () {
		list = labelList.GetComponentsInChildren<LabelTest>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void showScores(){
		int MaxScore = 0;
		int CurrScore = 0;
		foreach (LabelTest label in list) {
			MaxScore++;
			if (label.hasWear == label.wearTruth)
				CurrScore++;
		}
		Global.clearSystemInfo ();
		Global.addSystemInfo ("Your score: " + CurrScore + "/" + MaxScore + ".");
	}
}
