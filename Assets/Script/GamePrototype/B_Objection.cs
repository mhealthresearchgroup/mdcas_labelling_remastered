﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class B_Objection : MonoBehaviour {
	public GameObject graph;
	public bool isOjected = false;
	public Text buttonText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!isOjected)
			graph.transform.Translate (10 *　Vector3.left * Time.deltaTime);
	}

	public void click(){
		if (isOjected) {
			isOjected = false;
			buttonText.text = "Objection!";
		} else {
			isOjected = true;
			buttonText.text = "Continue.";
		}
	}
}
