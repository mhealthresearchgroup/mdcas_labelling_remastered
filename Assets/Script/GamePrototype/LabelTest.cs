﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LabelTest : MonoBehaviour {
	public bool hasWear;
	public bool wearTruth;
	// Use this for initialization
	void Start () {
		if (hasWear)
			this.gameObject.GetComponentInChildren<Text> ().text = "Wear";
		else this.gameObject.GetComponentInChildren<Text> ().text = "Non-Wear";
		this.GetComponent<Button> ().onClick.AddListener (changeWear);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void changeWear(){
		if (hasWear) {
			hasWear = false;
			this.gameObject.GetComponentInChildren<Text> ().text = "Non-Wear";
		} else {
			hasWear = true;
			this.gameObject.GetComponentInChildren<Text> ().text = "Wear";
		}
	}
}
