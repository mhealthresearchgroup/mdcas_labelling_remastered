﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using System.Threading.Tasks;
using Firebase.Unity.Editor;
using System.Collections.Generic;
using System;
//using YZL.Compress.GZip;
using System.Linq;

public class FirebaseController : MonoBehaviour {
	private DatabaseReference reference;
	private int id = 10;
	// Use this for initialization
	void Start () {
		// Set this before calling into the realtime database.
//		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://testbase-6e87a.firebaseio.com/");
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://testbase-6e87a.firebaseio.com/");
		reference = FirebaseDatabase.DefaultInstance.RootReference;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void uploadData(){
		writeNewUser ();
		id++;
	}
	public void retrieveData(){
		getUserData ();
		//writeNewUser ();
	}
		

	public void uploadRamdonData(){
		for (int i = 0; i < 20; i++) {
			writeRandomNewData ();
		}
	}

	public void writeRandomNewData(){
		DateTime currDate = DateTime.Now;
		DateTime startTime = currDate;
		DateTime endTime = currDate;
		startTime = startTime.AddMinutes(UnityEngine.Random.Range (0, 29));
		startTime = startTime.AddSeconds(UnityEngine.Random.Range (0, 29));
		endTime = endTime.AddMinutes(UnityEngine.Random.Range (30, 59));
		endTime = endTime.AddSeconds(UnityEngine.Random.Range (30, 59));

		int playerID = UnityEngine.Random.Range (1000, 2000);

		User user = new User (startTime, endTime, Global.label[UnityEngine.Random.Range(0, Global.label.Length)], playerID.ToString(), true);
		string json = JsonUtility.ToJson (user);
		string date = System.DateTime.Now.ToShortDateString ();
		date = date.Replace ("/", "_");
		//Debug.Log (date);
		reference.Child ("Cong").Child(date).Push ().SetRawJsonValueAsync (json);
	}

	public void writeLabels(float[,] labels, int length, int year, int month, int day){
		string path = year.ToString ("D4") + "/" + month.ToString ("D2") + "/" + day.ToString ("D2");
		for (int i = 0; i < length; i++){
			Label label = new Label (labels [i, 0], labels [i, 1], labels [i, 2], Global.labelType [Mathf.RoundToInt(labels [i, 3])], Global.username, DateTime.Now);
			string json = JsonUtility.ToJson (label);
			reference.Child(path).Push ().SetRawJsonValueAsync (json);
		}		
		
	}

	private void writeNewUser(){
//		User user = new User ("Cong", id, id * 2, id * 3);
//		string json = JsonUtility.ToJson (user);
//		reference.Child ("Users").Child (id+"").SetRawJsonValueAsync (json);
//		Global.addSystemInfo ("Upload successfully.");
	}

	private DataSnapshot snapshot;

	public void retrieveRandomData(){
		getRandomData ();
	}
	public int startTick, endTick;
	private void getRandomData(){
		startTick = Environment.TickCount;

		//get range
		getRange();

		//get data in the range
		getUserData ();
	}
		
//
//	public RegionController region;
	private string startRegionStr, endRegionStr;

	private void getRange(){
//		DateTime startDate = DateTime.Parse ("4_10_2017 2:00:00"), endDate = DateTime.Parse ("4_10_2017 2:21:00");
//		long allTicks = endDate.Ticks - startDate.Ticks;
//
//		Debug.Log (allTicks);
//		Debug.Log ((long)(allTicks * region.leftRegion));
//
//		DateTime startRegion = startDate.AddTicks ((long)(allTicks * region.leftRegion));
//		Debug.Log (startRegion);
//
//		DateTime endRegion = startDate.AddTicks ((long)(allTicks * region.rightRegion));
//		startRegionStr = startRegion.ToString().Replace ("/", "_");
//		endRegionStr = endRegion.ToString().Replace ("/", "_");
//		Debug.Log (startRegionStr + " " + endRegionStr);
	}

	public void getLabels(int year, int month, int day, RenderLabel labelController){
		string path = year.ToString ("D4") + "/" + month.ToString ("D2") + "/" + day.ToString ("D2");
		FirebaseDatabase.DefaultInstance
			.GetReference (path).GetValueAsync ().ContinueWith (task => {
				if (task.IsFaulted) {
					Debug.Log (task.Exception.Message);
				}

				if (task.IsCompleted) {
					int i = 0;
					Debug.Log("Get labels successfully. \nLabels count: " + task.Result.ChildrenCount);
					float[,] labels = new float[10000, 4];
					labelController.initLabels();
					foreach (DataSnapshot childSnapshot in task.Result.Children){
						string timeStamp =  childSnapshot.Child("timeStamp").Value.ToString();
						string startTime = childSnapshot.Child("startTime").Value.ToString();
						string endTime = childSnapshot.Child("endTime").Value.ToString();
						string labelType = childSnapshot.Child("labelType").Value.ToString();
//
						labels[i, 0] = float.Parse(timeStamp);
						labels[i, 1] = float.Parse(startTime);
						labels[i, 2] = float.Parse(endTime);

						if (!Global.labelType.Contains(labelType)){
							Global.labelType.Add (labelType);
						}

						for (int j = 0; j < Global.labelType.Count; j++) {
							if (labelType == Global.labelType[j]){
								labels[i, 3] = j;
								break;
							}
						}
							
						labelController.addLabel(labels[i, 0], labels[i, 1], labels[i, 2], (int)labels[i, 3]);
						i++;

					}


				}
		});
	}

	private void getUserData(){
//		string time = DateTime.Now.ToShortDateString ();
//		time = time.Replace ("/", "_");
//		DateTime tmp = DateTime.Now;
//		tmp.AddMinutes (-20);
//		string date = tmp.ToString ();
//		date = date.Replace ("/", "_");
//		Debug.Log (date);
//		FirebaseDatabase.DefaultInstance
//			.GetReference ("Cong/4_10_2017").OrderByChild ("endTime").EndAt (startRegionStr)
//			.GetValueAsync ().ContinueWith (task => {
//				if (task.IsFaulted) {
//					Debug.Log (task.Exception.Message);
//				} 
//				else if (task.IsCompleted) {
////					endTick = Environment.TickCount;
////					Debug.Log("Time cost:" + (endTick - startTick) +"ms");
////					Debug.Log(task.Result.ChildrenCount);
//
//					Global.labelList.Clear();
//
////					foreach (DataSnapshot childSnapshot in task.Result.Children){
////						string startTime = childSnapshot.Child("startTime").Value.ToString();
////						string endTime = childSnapshot.Child("endTime").Value.ToString();
////						Global.labelList.Add(new Label(startTime, endTime));
////						Global.addSystemInfo(Global.labelList.Last().start.ToShortTimeString() + Global.labelList.Last().end.ToShortTimeString());
////					}
//
//					long num = task.Result.ChildrenCount;
//
//					FirebaseDatabase.DefaultInstance
//						.GetReference ("Cong/4_10_2017").OrderByChild ("startTime").EndAt (endRegionStr)
//						.GetValueAsync ().ContinueWith (task2 => {
//							if (task2.IsFaulted) {
//								Debug.Log (task2.Exception.Message);
//							} 
//							else if (task2.IsCompleted) {
//								foreach (DataSnapshot childSnapshot in task2.Result.Children){
//									string startTime = childSnapshot.Child("startTime").Value.ToString();
//									string endTime = childSnapshot.Child("endTime").Value.ToString();
//									string labelName = childSnapshot.Child("labelName").Value.ToString();
//									Global.labelList.Add(new Label(startTime, endTime, labelName));
//
//
//								}
//								Global.labelList.RemoveRange(0, (int)num);
//								Debug.Log("number of label:" + Global.labelList.Count);
//								foreach (Label labelTmp in Global.labelList){
//									Global.addSystemInfo(labelTmp.start.ToShortTimeString() + " " +labelTmp.end.ToShortTimeString() + " " + labelTmp.labelName);
//								}
//							}
//						}
//					);
//
//
////					foreach(var childSnapshot in task.Result.Children){
////						Global.addSystemInfo(
////							childSnapshot.Child
////						);
////					}
//				//Dictionary list = snapshot.Children<User>();
//				}
//			}
//		);
	}

	public void dowloadFromStorage(){
		Firebase.Storage.StorageReference path_reference = Global.storage_ref.Child ("csv/data1.csv.gz");
		string local_url = "data1.csv.gz";
		Task task = path_reference.GetFileAsync (
			local_url, 
			new Firebase.Storage.StorageProgress<Firebase.Storage.DownloadState> (state => {
				Debug.Log (String.Format ("Progress: {0} bytes transferred.", state.BytesTransferred));
				}
			),
			System.Threading.CancellationToken.None);
		task.ContinueWith(resultTask => {
			if (!resultTask.IsFaulted && !resultTask.IsCanceled) {
				Debug.Log("Download finished. Start to unzip file.");
//				unzipFile();
			}
		});



//		Global.storage_ref.Child("images/mountains.jpg").GetFileAsync(
//			local_file, new StorageProgress(state => {
//				// called periodically during the download
//				DebugLog(String.Format("Progress: {0} of {1} bytes transferred.",
//					state.BytesTransferred, state.TotalByteCount));
//			}), CancellationToken.None);
//		task.ContinueWith(resultTask => {
//			if (!resultTask.IsFaulted && !resultTask.IsCancelled) {
//				DebugLog("Download finished.");
//			}
//		});
}
		

	//	void progress(Firebase.Storage.DownloadState state)({
//		Debug.Log(state.BytesTransferred + "/" + state.TotalByteCount);
//	}

//	public void unzipFile(){
////
//		for (int date = 4; date < 8; date++) {
//			for (int hour = 0; hour < 25; hour++) {
//				GZipFile.DeCompressAsync (getDataPathByDateAndHour(date, hour), makeDataPathByDateAndHour(date, hour), null);
//				GZipFile.DeCompressAsync (getLabelPathByDateAndHour(date, hour), makeLabelPathByDateAndHour(date, hour), null);
//			}
//		}
//
//
//		Debug.Log("Unzip done.");
//	}

	public string getDataPathByDateAndHour(int date, int hour){
		return "data/" + date.ToString("D2") + "/" + hour.ToString("D2") 
			+ "/ActigraphGT3XPLUS-AccelerationCalibrated-NA.CLE2B41120122-AccelerationCalibrated.2013-07-" 
			+ date.ToString("D2") + "-" + hour.ToString("D2") 
			+ "-00-00-000-M0400.sensor.csv.gz";
	}

	public string getLabelPathByDateAndHour(int date, int hour){
		return "data/" + date.ToString("D2") + "/" + hour.ToString("D2") + "/FreeLiving.algo-FreeLiving.2013-07-"
			+ date.ToString("D2") + "-" + hour.ToString("D2") 
			+ "-00-00-000-M0500.annotation.csv.gz";
	}


	public string makeDataPathByDateAndHour(int date, int hour){
		return  "data/" + date.ToString("D2") + "/" + hour.ToString("D2") 
			+ "/Data-"
			+ date.ToString("D2") + "-" + hour.ToString("D2") 
			+ ".csv";
	}


	public string makeLabelPathByDateAndHour(int date, int hour){
		return  "data/" + date.ToString("D2") + "/" + hour.ToString("D2") 
			+ "/Label-"
			+ date.ToString("D2") + "-" + hour.ToString("D2") 
			+ ".csv";
	}
}
