﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraView : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Orthographic Changed
		if (Input.GetKey(KeyCode.F1)){
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, 0));
		}

		if (Input.GetKey(KeyCode.F2)){
			transform.rotation = Quaternion.Euler (new Vector3 (0, -90, 0));
		}

		if (Input.GetKey(KeyCode.F3)){
			transform.rotation = Quaternion.Euler (new Vector3 (90, 0, 0));
		}
	}
}
